<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
   public $table="applicants";
   public $timestamps=false;
   public function Entrepreneurs(){
     
        return $this->belongsToMany('App\Entrepreneur');
       }

  public function Investors(){

        return $this->belongsToMany('App\Investor');
       }
  public function Startups(){

        return $this->belongsToMany('App\Startup');

       }
}
