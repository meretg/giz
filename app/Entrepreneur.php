<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrepreneur extends Model
{
  public $table="enterepreneus";
  public $timestamps=false;
  public function applicants(){

      return $this->belongsTo('App\Applicant');

  }
}
