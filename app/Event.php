<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  public $table="events";
  public $timestamps=false;
  public function eventPhotos()
  {
      return $this->hasMany('App\eventPhoto','eventId');
  }
}
