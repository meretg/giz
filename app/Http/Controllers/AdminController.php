<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\Entrepreneur;
use App\Investor;
use App\Startup;
use App\Admin;
use App\Team;
use App\Http\Requests;
use View;
use Auth;
use Redirect;
use  Carbon;
use Storage;
use Session;
use Hash;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
public function admin_login(Request $request){
  $errors="wrong email or password";
    $admins=Admin::all();
    $check=Auth::attempt(['email'=>$request->email,'password'=>$request->password]);

    if($check){

  //return View::make('admin_dashboard',compact('admins'));
  return redirect('admin_dashboard');
//  return json_encode($admins);

    }
      Session::flash('flash_message',$errors);
      return redirect()->back();
}
public function logout()
  {
     Auth::logout();
      $check=Auth::check();
      echo($check);
    return redirect ('admin_login');
  }

  public function viewAdmin()
    {
      if(!Auth::check()){
          return redirect ('admin_login');
      }
        else{
          $admins=Admin::all();
          $id=Auth::id();
          foreach($admins as $admin){
                if($admin->id==$id){
                  $admin->IsAdmin=True;
                }
                else{
                  $admin->IsAdmin=False;
                }
              }
            return view::make('admin_dashboard',compact('admins'));
        }

    }

  public function admin(Request $request){
    //add&update
    if(!Auth::check()){
        return redirect ('admin_login');
    }

    $admins=Admin::all();
        if ($request->isMethod('get')){
          if($request->id){
            $Admin=Admin::findorfail($request->id);

            $Admin->username=$request->username;
            $Admin->email=$request->email;
            $Admin->password=Hash::make($request->password);
            $Admin->save();
            $admins=Admin::all();
          }
          else{
            $newAdmin=new Admin;
            $newAdmin->username=$request->username;
            $newAdmin->email=$request->email;
            $newAdmin->password=Hash::make($request->password);
            $newAdmin->save();
            $admins=Admin::all();
          }

          return redirect('admin_dashboard')->with(['admins'=>$admins]);
        //  return redirect(route('admin_dashboard.'.$admins));
          //return redirect()->back();
        }


        // //delete
        if ($request->isMethod('post')){
          $Admin=Admin::findorfail($request->id);
          $Admin->delete();
          $admins=Admin::all();
        return redirect('admin_dashboard')->with(['admins'=>$admins]);
          //return redirect('admin_dashboard')->with(['admins'=>$admins]);
          //return json_encode('true');
            //return redirect(route('admin_dashboard.'.$admins));
        }
        // //view
        if ($request->isMethod('PUT')){
          $Admin=Admin::findorfail($request->id);
          return json_encode($Admin);

        }


  }
public function investor(Request $request){
   //view
   if ($request->isMethod('get')) {
      if($request->id){
        $investor=Investor::where('id',$request->id)->first();
      $appliant=Applicant::findorfail($investor->applicantId);
      if(!$appliant->DOB){
        $appliant->age="empty";
      }
       $appliant->age=$this->date($appliant->DOB);
        $investor->applicant=$appliant;
        return json_encode($investor);
      }
     else{
        $i=0;
       $Investors=Investor::all();
       foreach($Investors as $investor){
         $applicant=Applicant::findorfail($investor->applicantId);

         $investor->applicant=$applicant;
         $investors[$i++]=$investor;
      }
      return view::make('investors_dashboard',compact('investors'));

    }
}
  //delete
 if ($request->isMethod('post')) {
   $investor=Investor::where('id',$request->id)->first();
 $appliant=Applicant::findorfail($investor->applicantId);
   $appliant->delete();
   $i=0;
  $Investors=Investor::all();
  foreach($Investors as $investor){
    $applicant=Applicant::findorfail($investor->applicantId);
    $investor->applicant=$appliant;
    $investors[$i++]=$investor;

  }
  //return view::make('investors_dashboard',compact('investors'));
  return redirect('investors_dashboard')->with(['investors'=>$investors]);
}
}
public function Startup(Request $request){
   //view
   if ($request->isMethod('get')) {
      if($request->id){
        $startup=Startup::where('id',$request->id)->first();
      $appliant=Applicant::findorfail($startup->applicantId);
      if(!$appliant->DOB){
        $appliant->age="empty";
      }
      $appliant->age=$this->date($appliant->DOB);
         $startup->applicant=$appliant;
        return json_encode($startup);
      }
      else{
        $i=0;
       $startups=Startup::all();
       foreach($startups as $startup){

         $applicant=Applicant::findorfail($startup->applicantId);
         $startup->applicant=$applicant;
          $Startups[$i++]=$startup;
      }
   return view::make('startups_dashboard',compact('Startups'));
    //return json_encode($startup);
    }

   }

//delete
 if ($request->isMethod('post')) {

   $startup=Startup::where('id',$request->id)->first();
 $appliant=Applicant::findorfail($startup->applicantId);
   $appliant->delete();
   $i=0;
  $startups=Startup::all();
  foreach($startups as $startup){
    $applicant=Applicant::findorfail($startup->applicantId);
    $startup->applicant=$applicant;
    $Startups[$i++]=$startup;

  }
//  return view::make('startups_dashboard',compact('Startups'));
  return redirect('startups_dashboard')->with(['Startups'=>$Startups]);
}
}
public function Entrepreneur(Request $request){
   //view
   if ($request->isMethod('get')) {
      if($request->id){
        $entrepreneur=Entrepreneur::where('id',$request->id)->first();
      $appliant=Applicant::findorfail($entrepreneur->applicantId);

         $entrepreneur->applicant=$appliant;
        return json_encode($entrepreneur);
      }
      else{
        $i=0;
       $entrepreneurs=Entrepreneur::all();
       foreach($entrepreneurs as $entrepreneur){

         $applicant=Applicant::findorfail($entrepreneur->applicantId);
         $entrepreneur->applicant=$applicant;
          $Entrepreneurs[$i++]=$entrepreneur;
      }
   return view::make('Entrepreneurs_dashboard',compact('Entrepreneurs'));
    //return json_encode($startup);
    }

   }

//delete
 if ($request->isMethod('post')) {

   $entrepreneur=Entrepreneur::where('id',$request->id)->first();
   $appliant=Applicant::findorfail($entrepreneur->applicantId);
   $appliant->delete();
   $i=0;
  $entrepreneurs=Entrepreneur::all();
  foreach($entrepreneurs as $entrepreneur){
    $applicant=Applicant::findorfail($entrepreneur->applicantId);
    $entrepreneur->applicant=$applicant;
  $Entrepreneurs[$i++]=$entrepreneur;

  }
  //return view::make('Entrepreneurs_dashboard',compact('Entrepreneurs'));
    return redirect('Entrepreneurs_dashboard')->with(['Entrepreneurs'=>$Entrepreneurs]);
}
}
public function team(Request $request){
     //delete
   if ($request->isMethod('get')) {
      if($request->id){
        $team=Team::findorfail($request->id);
        $team->delete();
        $teams=Team::all();
        //return view::make('team_dashboard',compact('teams'));
        return redirect('team_dashboard')->with(['teams'=>$teams]);
      }
    //  view all
      else{
        $teams=Team::all();
    return view::make('team_dashboard',compact('teams'));
   }

}


    //edit
      if ($request->isMethod('post')){
          if($request->id){
            $team=Team::findorfail($request->id);
            $team->name=$request->name;
            if($request->photo){
              $image = Input::file('photo');
           //   if($request->file('image')){
           //    //  $team->pic=$request->photo;
           //
           //    return json_encode("true");
           // }
             $extension = $request->photo->getClientOriginalExtension();
              $fileSize = $image->getClientSize();
              if ($fileSize > 15728640)
                 {
              $image->resize(120,75);
                 }
              $imageFile = time() . '.' . $extension;

               Storage::put(
                         $imageFile,
                         file_get_contents($request->file('photo')->getRealPath())
                     );


              $team->pic=$imageFile;
          }
          else{

            $team->pic=$team->pic;
          }

            $team->phone=$request->phone;
            $team->type=$request->type;
            $team->bio=$request->bio;
            $team->email=$request->email;
            $team->facebook=$request->facebook;
            $team->linkedin=$request->linkedin;
          $team->save();
          $teams=Team::all();
        //return view::make('team_dashboard',compact('teams'));
        return redirect('team_dashboard')->with(['teams'=>$teams]);
          }
          //add
          else{
            $team=new Team;
            $team->name=$request->name;
          //   if($request->hasFile('photo')){
          //    //  $team->pic=$request->photo;
          //
          //    return json_encode("true");
          // }
            $image = Input::file('photo');
            $destinationPath = 'images/'; // upload path
            $extension = $request->photo->getClientOriginalExtension();
            $fileSize = $image->getClientSize();
            if ($fileSize > 15728640)
               {
            $image->resize(120,75);
               }
            $imageFile = time() . '.' . $extension;

             Storage::put(
                       $imageFile,
                       file_get_contents($request->file('photo')->getRealPath())
                   );


            $team->pic=$imageFile;
            $team->phone=$request->phone;
            $team->type=$request->type;
            $team->bio=$request->bio;
            $team->email=$request->email;
            $team->facebook=$request->facebook;
            $team->linkedin=$request->linkedin;
            $team->save();
            $teams=Team::all();
        //return view::make('team_dashboard',compact('teams'));
        return redirect('team_dashboard')->with(['teams'=>$teams]);
          }

 }
//view
 if ($request->isMethod('put')) {
$team=Team::findorfail($request->id);

  return json_encode($team);
}
}

public function date($DOB){
  $mytime = Carbon\Carbon::now();
  $applicant_date= Carbon\Carbon::createFromFormat('Y-m-d',$DOB)->year;
  $age=$mytime->year-$applicant_date;
  return json_encode($age);
}
public function view(){
  $team=Team::all();
return view::make('team',compact('team'));
//return json_encode($team);
//return redirect('team')->with(['team'=>$team]);

}
}
