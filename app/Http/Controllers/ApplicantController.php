<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Applicant;
use App\Entrepreneur;
use App\Investor;
use App\Startup;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Storage;
use Validator;
use Session;
use Redirect;

class ApplicantController extends Controller
{
    public function add_applicant(Request $request){
    //  $message= "تم التسجيل بنجاح";
    //   $this->validate($request, [
    //     'fullName' => 'required|unique:applicants|max:25',
    //     'DOB'=> 'required|date|date_format:Y-m-d',
    //     'launchDate'=> 'required|date|date_format:Y-m-d',
    //      'email' => 'required|email|unique:applicants',
    //       'phoneNo' => 'required|min:11|numeric',
    //       'whatsUpNo' => 'required|min:11|numeric',
    //      'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
    // ]);

    $validator = Validator::make($request->all(), [
          'fullName' => 'required|alpha',
          'DOB'=> 'sometimes|required|date_format:Y-m-d',
          'city' => 'required|alpha',
          'email' => 'required|email',
          'phoneNo' => 'required|numeric',
          'whatsUpNo' => 'numeric',
          'background' => 'required',
          'experienceField' => 'sometimes|required',
          'investBefore' => 'sometimes|required',
          'investementField' => 'sometimes|required',
          'investementIndustry' => 'sometimes|required',
          'investementNumber' => 'sometimes|required',
          'startUpName' => 'sometimes|required',
          'teamDetails' => 'sometimes|required',
          'ideaDescription' => 'sometimes|required',
          'launchDate'=> 'sometimes|required|date_format:Y-m-d',
          'website' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
          'facebook' => 'sometimes|required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
          'prototype' => 'sometimes|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
          'registered' => 'sometimes|required',
          'registeredAim' => 'sometimes|required',
          'bussinessPlan' =>  'sometimes|max:10000|mimes:doc,docx,pdf'
       ]);

       $validator->after(function($validator) {

        //$validator->errors()->add('fullName', 'invalid is wrong with this fullName!');

});
        if ($validator->fails()){

        return redirect()->back()->withErrors($validator);

        }

      $newApplicant=new Applicant;
      $newApplicant->fullName=$request->fullName;
      $newApplicant->email=$request->email;
      $newApplicant->phoneNo=$request->phoneNo;
      $newApplicant->whatsUpNo=$request->whatsUpNo;
      $newApplicant->city=$request->city;
      $newApplicant->background=$request->background;


      //$newApplicant->ideaDescription=$request->ideaDescription;
      // $newApplicant->facebookPage=$request->facebookPage;
      // $newApplicant->demo=$request->demo;
      // $newApplicant->prototype=$request->prototype;
      // $newApplicant->website=$request->website;
      //$newApplicant->bussinessPlan=$request->bussinessPlan;
      $newApplicant->save();
     if($request->type=='investor'){
        $newInvestor=new Investor;
        $newInvestor->investBefore=$request->investBefore;

        $newInvestor->investementField=$request->investementField;
        $newInvestor->investementIndustry=$request->investementIndustry;
        if($request->other){
            $newInvestor->other=$request->other;
        }

        $newInvestor->investementNumber=$request->investementNumber;
        $newInvestor->applicantId=$newApplicant->id;
        $newInvestor->save();
        $newApplicant->DOB=$request->DOB;
        $newApplicant->experienceField=$request->experienceField;
        $newApplicant->save();
        }
     if($request->type=='entrepreneur'){
        $newEntrepreneur=new Entrepreneur;
        $newEntrepreneur->education=$request->education;
        $newEntrepreneur->haveIdea=$request->haveIdea;
        $newEntrepreneur->suggestedBussinessType=$request->suggestedBussinessType;
        //$newEntrepreneur->other=$request->other;
        $newEntrepreneur->ideaStage=$request->ideaStage;
        $newEntrepreneur->haveTeam=$request->haveTeam;
        $newEntrepreneur->teamDetails=$request->teamDetails;
        $newEntrepreneur->registeredAim=$request->registeredAim;
        $newEntrepreneur->applicantId=$newApplicant->id;
        $newEntrepreneur->save();
        $newApplicant->ideaDescription=$request->ideaDescription;
        $newApplicant->facebookPage=$request->facebookPage;
        // $newApplicant->demo=$request->demo;
        $newApplicant->prototype=$request->prototype;
        // $newApplicant->website=$request->website;
        $newApplicant->DOB=$request->DOB;
        $newApplicant->experienceField=$request->experienceField;




        if( $request->bussinessPlan){
          Storage::put(
                          $request->bussinessPlan,
                          file_get_contents($request->file('bussinessPlan')->getRealPath())
                      );
       $newApplicant->bussinessPlan=$request->bussinessPlan;
         $newApplicant->save();
        }


   }
      if($request->type=='startup'){
        $newStartup=new Startup;
        $newStartup->startUpName=$request->startUpName;
        //$newStartup->address=$request->address;
        $newStartup->teamDetails=$request->teamDetails;
        $newStartup->employeeNo=$request->employeeNo;
        $newStartup->customers=$request->customers;
        $newStartup->launchDate=$request->launchDate;
        $newStartup->registered=$request->registered;
        //$newStartup->services=$request->services;
        $newStartup->registeredAim=$request->registeredAim;
        $newStartup->applicantId=$newApplicant->id;
        $newStartup->save();
        $newApplicant->DOB=$request->DOB;
        $newApplicant->experienceField=$request->experienceField;
        $newApplicant->website=$request->website;
        $newApplicant->prototype=$request->prototype;
        $newApplicant->facebookPage=$request->facebookPage;
        $newApplicant->ideaDescription=$request->ideaDescription;
        if( $request->bussinessPlan){
          Storage::put(
                          $request->bussinessPlan,
                          file_get_contents($request->file('bussinessPlan')->getRealPath())
                      );
       $newApplicant->bussinessPlan=$request->bussinessPlan;
         $newApplicant->save();
        }

      }
     //return json_encode($newInvestor->investementField);
     //   Session::flash('flash_message',$message);
       return redirect ('/');
     //return json_encode('true');

    }

}
