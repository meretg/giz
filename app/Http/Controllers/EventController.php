<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use App\Event;
use App\eventPhoto;
use View;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $events=Event::all();
      $result = array();
      foreach ($events as $event)
      {
          $record = new \stdClass();
          $record->id = $event->id;
          $record->title = $event->title;
          $record->description = $event->description;
          $record->link = $event->link;
          $record->from = $event->from;
          $record->to = $event->to;
          $record->registrationDeadline = $event->registrationDeadline;

          $record->pictures = array();
          $eventPictures = eventPhoto::where('eventId',$event->id)->get();
          foreach ($eventPictures as $pic)
          {
            array_push($record->pictures,$pic);
          }
          array_push($result,$record);
      }
      return view('events_dashboard', [
          'objects' => $result,
      ]);
      //return view::make('events_dashboard',compact('events'));

    }
    public function view(){
      $events=Event::all();
      $result = array();
      foreach ($events as $event)
      {
          $record = new \stdClass();
          $record->id = $event->id;
          $record->title = $event->title;
          $record->description = $event->description;
          $record->link = $event->link;
          $record->from = $event->from;
          $record->to = $event->to;
          $record->registrationDeadline = $event->registrationDeadline;

          $record->pictures = array();
          $eventPictures = eventPhoto::where('eventId',$event->id)->get();
          foreach ($eventPictures as $pic)
          {
            array_push($record->pictures,$pic);
          }
          array_push($result,$record);
      }
      // return view('news', [
      //     'events' => $result,
      // ]);
      return $result;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $event=new Event;
      $event->title=trim($request->title);
      $event->description=trim($request->description);
      $event->link=trim($request->link);
      $event->from=trim($request->from_date);
      $event->to=trim($request->to_date);
      $event->registrationDeadline=trim($request->deadline);
      $result = $event->save();
      if($result)
      {
        $valid = 0;
        if($request->hasFile('pictures'))
        {
          foreach($request->pictures as $file)
          {

           if($file->isValid())
            {
              $valid = 1;

              $destinationPath = 'events/'; // upload path
              $extension = $file->getClientOriginalExtension();
              $fileSize = $file->getClientSize();
              if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
              {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $valid = 0;
              }
            }
            if($valid)
            {
                // Check file size < 15MB
                if ($fileSize > 15728640)
                {
                    $file->resize(120,75);
                }

                $fileName = str_random(28) . time() . '.' . $extension;
                $file->move($destinationPath, $fileName);


                $photo=new eventPhoto;
                $photo->eventId=$event->id;
                $photo->pic = $fileName;
                $photo->save();
             }

           }
         }
      }

      Session::flash('flash_message', 'Event data has been added successfully');
      return redirect ()-> route ('admin.events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $event= Event::findOrFail($request->id);
      $photo=eventPhoto::where('eventId',$event->id)->get();
      $event->pic=$photo;

      if($request->ajax()){
        return json_encode($event);
      }
      else{
      return view::make('event_details',compact('event'));
        //return json_encode($event);
      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $event= Event::findOrFail($request->id);
      $event->title=trim($request->title);
      $event->description=trim($request->description);
      $event->phone=trim($request->phone);
      $event->link=trim($request->link);
      $event->from=trim($request->from_date);
      $event->to=trim($request->to_date);
      $event->registrationDeadline=trim($request->deadline);
      //$news->save();
      $result = $event->save();


      //Input::file()
      if($result)
      {
        $valid = 0;
        if($request->hasFile('picture'))
        {

          $file=$request->picture;

            if($file->isValid())
            {
              $valid = 1;

              $destinationPath = 'events/'; // upload path
              $extension = $file->getClientOriginalExtension();
              $fileSize = $file->getClientSize();
              if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
              {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $valid = 0;
              }
            }
            if($valid)
            {
                // Check file size < 15MB
                if ($fileSize > 15728640)
                {
                    $file->resize(120,75);
                }

                $fileName = str_random(28) . time() . '.' . $extension;
                $file->move($destinationPath, $fileName);


                $photo=new eventPhoto;
                $photo->eventId=$request->id;
                $photo->pic = $fileName;
                $photo->save();
             }

           }

     }


      Session::flash('flash_message', 'Event data has been updated successfully');
      return redirect ()-> route ('admin.events');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $events= Event::findOrFail($request->id);
      $events->delete();
      Session::flash('flash_message', 'Event data has been deleted successfully');
      return redirect ()-> route ('admin.events');
    }
}
