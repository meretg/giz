<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use App\News;
use App\Photo;
use App\Event;
use App\eventPhoto;
use View;
use App\Http\Controllers\EventController;
class NewsController extends Controller
{
  public function index()
  {
    $allNews=News::all();
    $result = array();

    foreach ($allNews as $news)
    {
        $record = new \stdClass();
        $record->id = $news->id;
        $record->title = $news->title;
        $record->description = $news->description;
        $record->link = $news->link;

        $mainImg = Photo::find($news->mainImg);

        $record->mainImg = $mainImg ? $mainImg : '';

        $record->pictures = array();
        $newsPictures = Photo::where('newsId',$news->id)->get();
        foreach ($newsPictures as $pic)
        {
          if($mainImg->id != $pic->id)
          {
            array_push($record->pictures,$pic);
          }
        }
        array_push($result,$record);
    }

    return view('news_dashboard', [
        'objects' => $result,
    ]);
  }
  public function view(){
    $event= new EventController();
    $events=$event->view();
    $allNews=News::all();
    $result = array();

    foreach ($allNews as $news)
    {
        $record = new \stdClass();
        $record->id = $news->id;
        $record->title = $news->title;
        $record->description = $news->description;
        $record->link = $news->link;
        $record->mainImg = Photo::findOrFail($news->mainImg);

        // $record->pictures = array();
        // $newsPictures = Photo::where('newsId',$news->id)->get();
        // foreach ($newsPictures as $pic)
        // {
        //   array_push($record->pictures,$pic);
        // }
        array_push($result,$record);
    }

    return view('news', [
        'objects' => $result,
        'events' => $events,
    ]);
  }

  public function store(Request $request)
  {
    // $this->validate($request,[
    // //'logo'=>'Regex:/^([A-Za-z._\-\'\s\p{Arabic}])+$/u',
    // 'title'=>'required|Regex:/^([A-Za-z._\-\'\s\p{Arabic}])+$/u',
    // 'description'=>'required|Regex:/^([A-Za-z._\-\'\s\p{Arabic}])+$/u'
    // ]);

    //dd($request->allFiles());

      $news=new News;
      $news->title=trim($request->title);
      $news->description=trim($request->description);
      $news->link=trim($request->link);
      $news->save();

      $valid = 0;
      if($request->hasFile('mainImg'))
      {
          $file = $request->mainImg;
          if($file->isValid())
          {
            $valid = 1;
            $destinationPath = 'NewsPictures/'; // upload path
            $extension = $file->getClientOriginalExtension();
            $fileSize = $file->getClientSize();
            if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
            {
              //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
              $valid = 0;
            }
          }
          if($valid)
          {
              // Check file size < 15MB
              if ($fileSize > 15728640)
              {
                  $file->resize(120,75);
              }

              $fileName = str_random(28) . time() . '.' . $extension;
              $file->move($destinationPath, $fileName);

              $photo=new Photo;
              $photo->newsId=$news->id;
              $photo->pic = $fileName;
              $photo->save();
              $news->mainImg = $photo->id;
           }

         }

      $result = $news->save();
      if($result)
      {
        $valid = 0;
        if($request->hasFile('pictures'))
        {
          foreach($request->pictures as $file)
          {

            if($file->isValid())
            {
              $valid = 1;

              $destinationPath = 'NewsPictures/'; // upload path
              $extension = $file->getClientOriginalExtension();
              $fileSize = $file->getClientSize();
              if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
              {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $valid = 0;
              }
            }
            if($valid)
            {
                // Check file size < 15MB
                if ($fileSize > 15728640)
                {
                    $file->resize(120,75);
                }

                $fileName = str_random(28) . time() . '.' . $extension;
                $file->move($destinationPath, $fileName);

                $photo=new Photo;
                $photo->newsId=$news->id;
                $photo->pic = $fileName;
                $photo->save();
             }

           }
         }
      }

      Session::flash('flash_message', 'News data has been added successfully');
      return redirect ()-> route ('admin.news');
  }

  public function show(Request $request)
  {
    $result=array();
      $news= News::findOrFail($request->id);
      if($request->ajax()){
        return json_encode($news);
      }
      else
      {
        $record = new \stdClass();
        $record->id = $news->id;
        $record->title = $news->title;
        $record->description = $news->description;
        $record->link = $news->link;

        $mainImg = Photo::find($news->mainImg);

        $record->mainImg = $mainImg ? $mainImg : '';

        $record->pictures = array();
        $newsPictures = Photo::where('newsId',$news->id)->get();
        foreach ($newsPictures as $pic)
        {
          if($mainImg->id != $pic->id)
          {
            array_push($record->pictures,$pic);
          }
        }
        array_push($result,$record);
      }
     return view::make('news_details',['news' => $record]);
  //  return view::make('news_details',compact('result'));
// return json_encode($result);
  }

  public function update(Request $request)
  {
    //validation

    $news= News::findOrFail($request->id);
    $news->title=trim($request->title);
    $news->description=trim($request->description);
    $news->link=trim($request->link);

    $valid = 0;
    if($request->hasFile('mainImg'))
    {
        $file = $request->mainImg;
        if($file->isValid())
        {
          $valid = 1;

          $destinationPath = 'NewsPictures/'; // upload path
          $extension = $file->getClientOriginalExtension();
          $fileSize = $file->getClientSize();
          if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
          {
            //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $valid = 0;
          }
        }
        if($valid)
        {
            // Check file size < 15MB
            if ($fileSize > 15728640)
            {
                $file->resize(120,75);
            }

            $fileName = str_random(28) . time() . '.' . $extension;
            $file->move($destinationPath, $fileName);

            $photo=new Photo;
            $photo->newsId=$news->id;
            $photo->pic = $fileName;
            $photo->save();

            $oldMainImg = Photo::find($news->mainImg);
            if($oldMainImg)
            {
              $oldMainImg->delete();
            }

            $news->mainImg = $photo->id;
         }

     }

    $result = $news->save();
    $photos=$request->pictures;

    //Input::file()
    if($result)
    {
      $valid = 0;
      if($request->hasFile('pictures'))
      {

        foreach($photos as $file)
        {

          if($file->isValid())
          {
            $valid = 1;

            $destinationPath = 'NewsPictures/'; // upload path
            $extension = $file->getClientOriginalExtension();
            $fileSize = $file->getClientSize();
            if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
            {
              //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
              $valid = 0;
            }
          }
          if($valid)
          {
              // Check file size < 15MB
              if ($fileSize > 15728640)
              {
                  $file->resize(120,75);
              }

              $fileName = str_random(28) . time() . '.' . $extension;
              $file->move($destinationPath, $fileName);

              $photo=new Photo;
              $photo->newsId=$request->id;
              $photo->pic = $fileName;
              $photo->save();
           }

         }
    }
   }


    Session::flash('flash_message', 'News data has been updated successfully');
    return redirect ()-> route ('admin.news');


  }

  public function destroy(Request $request)
  {
      $news= News::findOrFail($request->id);
      $news->delete();
      Session::flash('flash_message', 'News data has been deleted successfully');
      return redirect ()-> route ('admin.news');
  }
    public function deletePhoto(Request $request){
      $news=Photo::where([['newsId',$request->id],['id',$request->photoId]])->first();
      $news->delete();
      return json_encode(true);
    }
}
