<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use App\Partner;
use View;
use File;
class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $partners=Partner::all();

      // return view('partners_dashboard', [
      //     'partners' => $partners,
      // ]);
      return view::make('Partners_dashboard',compact('partners'));
    }
    public function view(){
      $partners=Partner::all();

      // return view('partners_dashboard', [
      //     'partners' => $partners,
      // ]);
      return view::make('partners',compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $file = Input::file('logo');
      $valid = 0;
      if($request->hasFile('logo') && $request->file('logo')->isValid())
      {
        $valid = 1;

        $destinationPath = 'PartnersLogo/'; // upload path
        $extension = $request->logo->getClientOriginalExtension();
        $fileSize = $file->getClientSize();
        if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
        {
          //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $valid = 0;
        }
      }
      if($valid)
      {
          // Check file size < 15MB
          if ($fileSize > 15728640)
          {
              $file->resize(120,75);
          }

          $fileName = time() . '.' . $extension;
          $file->move($destinationPath, $fileName);

          $partner=new Partner;
          $partner->title=trim($request->title);
          $partner->logo = $fileName;
          $partner->phone=trim($request->phone);
          $partner->bio=trim($request->bio);
          $partner->email=trim($request->email);
          $partner->facebook=trim($request->facebook);
          $partner->linkedin=trim($request->linkedin);
          $partner->save();
        }
        else
        {
          $partner=new Partner;
          $partner->title=trim($request->title);
          $partner->phone=trim($request->phone);
          $partner->bio=trim($request->bio);
          $partner->email=trim($request->email);
          $partner->facebook=trim($request->facebook);
          $partner->linkedin=trim($request->linkedin);
          $partner->save();
        }

      Session::flash('flash_message', 'Partner data has been added successfully');
      return redirect ()-> route ('admin.partner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $partner= Partner::findOrFail($request->id);
      if($request->ajax()){
        return json_encode($partner);
      }
      else{
        return view::make('view_partner',compact('partner'));
      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $partner= Partner::findOrFail($request->id);

      $file = Input::file('logo');
      $valid = 0;
      if($request->hasFile('logo') && $request->file('logo')->isValid())
      {
        $valid = 1;

        $destinationPath = 'PartnersLogo/'; // upload path
        $extension = $request->logo->getClientOriginalExtension();
        $fileSize = $file->getClientSize();
        if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
        {
          //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $valid = 0;
        }
      }
      if($valid)
      {
          // Check file size < 15MB
          if ($fileSize > 15728640)
          {
              $file->resize(75,75);
          }

          $fileName = time() . '.' . $extension;
          $file->move($destinationPath, $fileName);
          // if($portfolio->logo)
          // {
          //   unlink(public_path(). 'PortfolioLogo/' . $portfolio->logo->file);
          // }
          $image_path = 'PartnersLogo/' . $partner->logo;
          if(File::exists($image_path)) {
                File::delete($image_path);
          }
          $partner->logo = $fileName;
        }

        $partner->title=trim($request->title);
        $partner->phone=trim($request->phone);
        $partner->bio=trim($request->bio);
        $partner->email=trim($request->email);
        $partner->facebook=trim($request->facebook);
        $partner->linkedin=trim($request->linkedin);
        $partner->save();


      Session::flash('flash_message', 'Partner data has been updated successfully');
      return redirect ()-> route ('admin.partner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $partner= Partner::findOrFail($request->id);
      $partner->delete();
      Session::flash('flash_message', 'Partner data has been deleted successfully');
      return redirect ()-> route ('admin.partner');
    }
}
