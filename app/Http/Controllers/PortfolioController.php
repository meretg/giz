<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use App\Portfolio;
use File;

class PortfolioController extends Controller
{
  public function index()
  {
    $portfolios=Portfolio::all();

    return view('portfolio_dashboard', [
        'portfolios' => $portfolios,
    ]);
  }
    public function view(){
      $portfolios=Portfolio::all();

      return view('portfolio', [
          'portfolios' => $portfolios,
      ]);
    }

  public function store(Request $request)
  {
    // $this->validate($request,[
    // //'logo'=>'Regex:/^([A-Za-z._\-\'\s\p{Arabic}])+$/u',
    // 'title'=>'required|Regex:/^([A-Za-z._\-\'\s\p{Arabic}])+$/u',
    // 'description'=>'required|Regex:/^([A-Za-z._\-\'\s\p{Arabic}])+$/u'
    // ]);

    $file = Input::file('logo');
    $valid = 0;
    if($request->hasFile('logo') && $request->file('logo')->isValid())
    {
      $valid = 1;

      $destinationPath = 'PortfolioLogo/'; // upload path
      $extension = $request->logo->getClientOriginalExtension();
      $fileSize = $file->getClientSize();
      if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
      {
        //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $valid = 0;
      }
    }
    if($valid)
    {
        // Check file size < 15MB
        if ($fileSize > 15728640)
        {
            $file->resize(120,75);
        }

        $fileName = time() . '.' . $extension;
        $file->move($destinationPath, $fileName);

        $portfolio=new Portfolio;
        $portfolio->title=trim($request->title);
        $portfolio->logo = $fileName;
        $portfolio->phone=trim($request->phone);
        $portfolio->bio=trim($request->bio);
        $portfolio->email=trim($request->email);
        $portfolio->facebook=trim($request->facebook);
        $portfolio->linkedin=trim($request->linkedin);
        $portfolio->save();
      }
      else
      {
        $portfolio=new Portfolio;
        $portfolio->title=trim($request->title);
        $portfolio->phone=trim($request->phone);
        $portfolio->bio=trim($request->bio);
        $portfolio->email=trim($request->email);
        $portfolio->facebook=trim($request->facebook);
        $portfolio->linkedin=trim($request->linkedin);
        $portfolio->save();
      }

    Session::flash('flash_message', 'Portfolio data has been added successfully');
    return redirect ()-> route ('admin.portfolio');
  }

  public function show(Request $request)
  {
      $portfolio= Portfolio::findOrFail($request->id);
      if($request->ajax()){
          return json_encode($portfolio);
      }
      else{
        return view::make('portfolio',compact('portfolio'));
      }


  }

  public function update(Request $request)
  {
    //validation

    $portfolio= Portfolio::findOrFail($request->id);

    $file = Input::file('logo');
    $valid = 0;
    if($request->hasFile('logo') && $request->file('logo')->isValid())
    {
      $valid = 1;

      $destinationPath = 'PortfolioLogo/'; // upload path
      $extension = $request->logo->getClientOriginalExtension();
      $fileSize = $file->getClientSize();
      if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
      {
        //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $valid = 0;
      }
    }
    if($valid)
    {
        // Check file size < 15MB
        if ($fileSize > 15728640)
        {
            $file->resize(75,75);
        }

        $fileName = time() . '.' . $extension;
        $file->move($destinationPath, $fileName);
        // if($portfolio->logo)
        // {
        //   unlink(public_path(). 'PortfolioLogo/' . $portfolio->logo->file);
        // }
        $image_path = 'PortfolioLogo/' . $portfolio->logo;
        if(File::exists($image_path)) {
              File::delete($image_path);
        }
        $portfolio->logo = $fileName;
      }

      $portfolio->title=trim($request->title);
      $portfolio->phone=trim($request->phone);
      $portfolio->bio=trim($request->bio);
      $portfolio->email=trim($request->email);
      $portfolio->facebook=trim($request->facebook);
      $portfolio->linkedin=trim($request->linkedin);
      $portfolio->save();


    Session::flash('flash_message', 'Portfolio data has been updated successfully');
    return redirect ()-> route ('admin.portfolio');
  }

  public function destroy(Request $request)
  {
      $portfolio= Portfolio::findOrFail($request->id);
      $portfolio->delete();
      Session::flash('flash_message', 'Portfolio data has been deleted successfully');
      return redirect ()-> route ('admin.portfolio');
  }
}
