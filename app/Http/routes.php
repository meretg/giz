<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
],
function()
{
//   Route::get(LaravelLocalization::transRoute('investor'), function() {
//   return View::make('basic_form');
// });
Route::get('/', function () {
    return view('index');
});
Route::get('/contactus', function () {
    return view('contactus');

});
Route::get('investor', function () {
    return view('basic_form');
});
// Route::get('team', function () {
//     return view('team');
// });
// Route::get('news', function () {
//     return view('news');
// });
Route::get('news_details', function () {
    return view('news_details');
});
Route::get('event_details', function () {
    return view('event_details');
});
Route::get('investors', function () {
    return view('investors');
});
Route::get('portfolio', function () {
    return view('portfolio');
});
Route::get('/aboutus', function () {
    return view('aboutus');

});

Route::get('news_details','NewsController@show');
Route::get('event_details','EventController@show');
//Route::get('event_details/{id}','EventController@show');
Route::get('/Team','AdminController@view');
Route::get('news','NewsController@view');
Route::get('view','EventController@view');
Route::get('portfolio','PortfolioController@view');
Route::get('/entrepreneur', function () {
    return view('Enterpreter_form');
});
Route::get('/enter', function () {
    return view('enterpreter');

});
Route::post('basic_form','ApplicantController@add_applicant');
Route::post('basic','ApplicantController@add_applicant');
});


Route::get('/master', function () {
    return view('admin_master');
});
Route::get('/admin_login', function () {
    return view('admin_login');
});
Route::get('/investors_dashboard', function () {
    return view('investors_dashboard');
});
Route::get('/admin_dashboard', function () {
    return view('admin_dashboard');
})->name('admin_dashboard');
Route::get('/team_dashboard', function () {
    return view('team_dashboard');
});

Route::get('/news_dashboard', function () {
    return view('news_dashboard');
});
Route::get('/startups_dashboard', function () {
    return view('startups_dashboard');
});
Route::get('/events_dashboard', function () {
    return view('events_dashboard');
});

// Route::get('/partner_dashboard', function () {
//     return view('partner_dashboard');
// });


// Route::get('/admin_dashboard', function () {
//     return view('admin_dashboard');
// })->name('admin_dashboard');
//add_applicant

//teams
Route::get('team_dashboard','AdminController@team');
Route::put('view.member','AdminController@team');
Route::post('add_member','AdminController@team');
Route::post('edit_member','AdminController@team');
Route::get('delete_member','AdminController@team');
Route::get('team','AdminController@view');

//Entrepreneurs
Route::get('Entrepreneurs_dashboard','AdminController@Entrepreneur');
Route::get('Entrepreneur.view','AdminController@Entrepreneur');
Route::post('Entrepreneur.delete','AdminController@Entrepreneur')->name('delete_Entrepreneur');
//investors
Route::get('investors_dashboard','AdminController@investor');
Route::get('investors.view','AdminController@investor');
Route::post('investor.delete','AdminController@investor')->name('delete_investor');
//startups
Route::get('startups_dashboard','AdminController@Startup');
Route::get('startup.view','AdminController@Startup');
Route::post('startup.delete','AdminController@Startup')->name('delete_startup');

//Route::get('delete','AdminController@investor');
//admins
Route::post('admin_login','AdminController@admin_login');
Route::get('admin_logout','AdminController@logout');
Route::get('admin_dashboard','AdminController@viewAdmin');
Route::PUT('admin.view','AdminController@admin');
Route::get('add_admin','AdminController@admin')->name('add_admin');
Route::get('edit_admin','AdminController@admin')->name('edit_admin');

Route::post('admin.delete','AdminController@admin')->name('delete_admin');
//  Route::resource('admins', 'adminController',['only' => [
//     'index', 'store'
// ]]);admin/news#
// Route::resource('admins', 'adminController', ['names' => [
//     'store' => 'admin.add'
// ]]);

Route::get('/admin/portfolio',[
			'as' => 'admin.portfolio', 'uses' => 'PortfolioController@index'
		]);
Route::get('/admin/partner',[
    			'as' => 'admin.partner', 'uses' => 'PartnerController@index'
    		]);
Route::get('/admin/news',[
			'as' => 'admin.news', 'uses' => 'NewsController@index'
		]);
Route::get('/admin/events',[
    			'as' => 'admin.events', 'uses' => 'EventController@index'
    		]);

//Portfolio Controller

Route::group(['prefix' => 'portfolio'], function () {
  Route::post('show','PortfolioController@show');
  Route::post('view','PortfolioController@view');
  Route::post('create','PortfolioController@store');
  Route::post('update','PortfolioController@update');
  Route::post('delete','PortfolioController@destroy');
});
Route::group(['prefix' => 'partner'], function () {
  Route::post('show','PartnerController@show');
  Route::post('view','PartnerController@view');
  Route::post('create','PartnerController@store');
  Route::post('update','PartnerController@update');
  Route::post('delete','PartnerController@destroy');
});
//News Controller

Route::group(['prefix' => 'news'], function () {
  Route::post('show','NewsController@show');
  Route::post('view','NewsController@view');
  Route::post('create','NewsController@store');
  Route::post('update','NewsController@update');
  Route::post('delete','NewsController@destroy');
  Route::post('deletePhoto','NewsController@deletePhoto');
});

Route::group(['prefix' => 'events'], function () {
  Route::post('show','EventController@show');
  Route::post('view','EventController@view');
  Route::post('create','EventController@store');
  Route::post('update','EventController@update');
  Route::post('delete','EventController@destroy');

});
