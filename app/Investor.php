<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
  public $table="investors";
  public $timestamps=false;
  public function applicants(){

      return $this->belongsTo('App\Applicant');

  }
}
