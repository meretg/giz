<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table="news";
    public function photos()
    {
        return $this->hasMany('App\Photo','newsId');
    }
}
