<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
  public $table="photos";
  public function news()
  {
    return $this->belongsTo('App\News','newsId');
  }
}
