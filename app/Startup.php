<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Startup extends Model
{
  public $table="startups";
  public $timestamps=false;
  public function applicants(){

      return $this->belongsTo('App\Applicant');

  }
}
