<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class eventPhoto extends Model
{
  public $table="eventPhotos";
  public function news()
  {
    return $this->belongsTo('App\Event','eventId');
  }
}
