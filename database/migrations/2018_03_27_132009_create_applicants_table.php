<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
          $table->increments('id');
          $table->string('fullName');
          $table->string('email');
          $table->date('DOB');
          $table->string('phoneNo');
          $table->string('whatsUpNo');
          $table->string('background');
          $table->enum('experienceField',['finacialServices','marketing','operations',
          'sales','businessDevelopment','researchDevelopment','publicRelations']);
          $table->string('other');
          $table->string('city');
          $table->text('ideaDescription');
          //$table->enum('ideaLinks',['facebook','demo','prototype']);
          $table->text('facebookPage');
          $table->text('demo');
          $table->text('prototype');
          $table->text('website');
          $table->text('bussinessPlan');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applicants');
    }
}
