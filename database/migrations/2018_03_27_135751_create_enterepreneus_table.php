<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterepreneusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterepreneus', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('education',['none','student','graduated']);
            $table->enum('haveIdea',['none','yes','no']);
            $table->enum('suggestedBussinessType',['none','merchandisingBusiness','manufacturingBusiness',
            'servicesSolutions','technologyBasedSolution','agriculturalBusiness']);
            $table->text('other');
            $table->enum('ideaStage',['none','justIdea','feasibilityStudy','Prototype','readyToLunching']);
            $table->enum('haveTeam',['none','yes','no']);
            $table->text('teamDetails');
            $table->enum('registeredAim',['none','needInvestor','needConsultation']);
            $table->integer('applicantId')->unsigned();
            $table->foreign('applicantId')->references('id')->on('applicants')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enterepreneus');
    }
}
