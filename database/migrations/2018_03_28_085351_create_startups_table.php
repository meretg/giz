<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('startUpName');
            //$table->string('address');
            $table->text('teamDetails');
            $table->integer('employeeNo');
            $table->text('customers');
            $table->date('launchDate');
            $table->enum('registered',['','yes','no']);
            //$table->text('services');
            $table->enum('registeredAim',['','needInvestment','needConsultation','needguidance']);
            $table->integer('applicantId')->unsigned();
            $table->foreign('applicantId')->references('id')->on('applicants')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('startups');
    }
}
