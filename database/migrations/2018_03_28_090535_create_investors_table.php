<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investors', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('investBefore',['','yes','no']);
            //$table->text('previousInvestement');
            $table->enum('investementField',['','merchandisingBusiness','manufacturingBusiness','servicesSolutions',
            'technologyBasedSolution','agriculturalBusiness','production']);
            $table->enum('investementIndustry',['','healthcare','education','software','hardware','agriculture',
            'telecommunication','fastMovingGoods','media','industrial','e-Commerce','energy']);
            $table->text('other');
            $table->enum('investementNumber',['','10000-25000','26000-50000','51000-75000',
            '76000-100000','150000-250000','300000-400000','500000-600000','700000-800000','900000-1000000',
            '1500000-2500000','300000-5000000','5000000-10000000','>10Million']);
            $table->integer('applicantId')->unsigned();
            $table->foreign('applicantId')->references('id')->on('applicants')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investors');
    }
}
