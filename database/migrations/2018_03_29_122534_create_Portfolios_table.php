<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('portfolios', function (Blueprint $table) {
      $table->increments('id');
      $table->string('title');
      $table->string('logo');
      $table->string('phone');
      $table->text('bio');
      $table->text('email');
      $table->text('facebook');
      $table->text('linkedin');
      $table->timestamps();
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('portfolios');
    }
}
