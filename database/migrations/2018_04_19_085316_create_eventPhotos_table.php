<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('eventPhotos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('pic');
          $table->integer('eventId')->unsigned();
          $table->foreign('eventId')->references('id')->on('events')->onUpdate('cascade')->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventPhotos');
    }
}
