<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('admins')->insert([
            'username' => 'mai',
            'email' => 'mai@mai.com',
            'password' => bcrypt("mai123"),
        ]);
        DB::table('applicants')->insert([
              'fullName' => 'noor',
              'email' => 'noor@noor.com',

          ]);
        DB::table('applicants')->insert([
                'fullName' => 'rehab',
                'email' => 'rehab@rehab.com',

            ]);
        DB::table('applicants')->insert([
                  'fullName' => 'abeer',
                  'email' => 'abeer@abeer.com',

              ]);
        DB::table('applicants')->insert([
                    'fullName' => 'heba',
                    'email' => 'heba@heba.com',

                ]);
          DB::table('startups')->insert([
            'launchDate' => '2018-04-03',
            'registered' => 'yes',
            'applicantId' => '1',
                ]);
          DB::table('investors')->insert([
            'investBefore' => 'yes',
            'investementField' => 'production',
            'investementIndustry' => 'education',
            'investementNumber'=>'10000-25000',
            'applicantId' => '2',
                ]);
          DB::table('enterepreneus')->insert([
                  'education' => 'student',
                  'haveIdea' => 'yes',
                  'suggestedBussinessType' => 'merchandisingBusiness',
                  'ideaStage'=>'justIdea',
                  'haveTeam' => 'yes',
                  'registeredAim' => 'needInvestor',
                  'applicantId' => '3',
              ]);
          DB::table('teams')->insert([
                      'name' => 'noor',
                      'type' => 'investor',
                  ]);

    }
}
