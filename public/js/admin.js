function validateUsername() {

    var username =document.getElementById("admin_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("admin_nameinvalid").innerHTML="please enter you username";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("admin_nameinvalid").innerHTML="username is very long";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("admin_nameinvalid").innerHTML="username must not has numbers or special character";
        return false;

    }
     else
     {

        document.getElementById("admin_nameinvalid").innerHTML="";
        return true;

    }

}
function validatepassword()
{
  var password=document.getElementById("admin_password").value;
  if (password.length < 8)
  {
       document.getElementById("admin_passwordinvalid").innerHTML="must be at least 8 characters";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("admin_passwordinvalid").innerHTML=" must contain at least one letter.";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("admin_passwordinvalid").innerHTML=" must contain at least one digit.";
       return false;
   }
   else
   document.getElementById("admin_passwordinvalid").innerHTML="";
   return true;
}
function validateconfirmpass()
{
  if($("#admin_password").val()==$("#admin_password2").val())
  return true;
  else {
    $("#admin_repasswordinvalid").html("Passwords not match");
    return false;
  }
}
function validateEmail()
{

  var y = document.getElementById("admin_email").value;
  var atpos = y.indexOf("@");
  var dotpos = y.lastIndexOf(".");

  if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=y.length)) {

      document.getElementById("admin_emailinvalid").innerHTML="";
      return true;
  }
  else
  {

        document.getElementById("admin_emailinvalid").innerHTML="Invalid email";
      return false;
  }
}
function validate()
{
  if(validateUsername()==true && validatepassword()==true && validateEmail()==true && validateconfirmpass()==true )
  {
  return true;
}
  else
  {
  return false;
}
}
function validatetitle()
{
  var title =document.getElementById("title").value;
  if (title ==="") {
      document.getElementById("title_invalid").innerHTML="please enter title";
      return false;
    }
  else if ((title.length > 150)) {
      document.getElementById("title_invalid").innerHTML="title is very long";
      return false;
    }
    else
    {

       document.getElementById("title_invalid").innerHTML="";
       return true;

   }

}
function validatedes()
{
  var discription =document.getElementById("description").value;
  if (discription ==="") {
      document.getElementById("invalid_des").innerHTML="please enter description";
      return false;
    }
else{
       document.getElementById("invalid_des").innerHTML="";
       return true;

   }

}
function validatenews()
  {
    if(validatetitle()==true && validatedes()==true )
    {
    return true;
  }
    else
    {
    return false;
  }
  }
  function validatename_member()
  {
    var member_name =document.getElementById("member_name").value;
    if (member_name ==="") {
        document.getElementById("member_nameinvalid").innerHTML="please enter member name";
        return false;
      }
  else{
         document.getElementById("member_nameinvalid").innerHTML="";
         return true;

     }
  }
  function validateEmail_member()
  {

    var y = document.getElementById("member_email").value;
    var atpos = y.indexOf("@");
    var dotpos = y.lastIndexOf(".");

    if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=y.length)) {

        document.getElementById("member_emailinvalid").innerHTML="";
        return true;
    }
    else
    {

          document.getElementById("member_emailinvalid").innerHTML="Invalid email";
        return false;
    }
  }
// function validatephone_member()
// {
//   var member_phone = document.getElementById(member_phone).value;
//
//    var intRegex = "/[0-9 -()+]+$/";
// if((member_phone.length < 11) || (!intRegex.test(member_phone)))
// {
//    alert('Please enter a valid phone number.');
//    return false
//
// }
function validate_team()
{
  if(validatephone_member()==true && validateEmail_member()==true && validatename_member()==true )
  {
  return true;
}
  else
  {
  return false;
}
}
function validateportfiliotitle()
{
  var portfiliotitle =document.getElementById("portfiliotitle").value;
  if (portfiliotitle ==="") {
      document.getElementById("invalid_portfiliotitle").innerHTML="please enter title";
      return false;
    }
  else if ((portfiliotitle.length > 150)) {
      document.getElementById("invalid_portfiliotitle").innerHTML="title is very long";
      return false;
    }
    else
    {

       document.getElementById("invalid_portfiliotitle").innerHTML="";
       return true;

   }

}
function validateEmail_portfilio()
{

  var y = document.getElementById("portfilioemail").value;
  var atpos = y.indexOf("@");
  var dotpos = y.lastIndexOf(".");

  if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=y.length)) {

      document.getElementById("invalid_portfilioemail").innerHTML="";
      return true;
  }
  else
  {

        document.getElementById("invalid_portfilioemail").innerHTML="Invalid email";
      return false;
  }
}
function validateportfilio()
{
  if(validateEmail_portfilio()==true && validateportfiliotitle()==true)
  {
    return true;
  }
  else {
    return false;
  }
}
function validateeventtitle()
{
  var event_title =document.getElementById("event_title").value;
  if (event_title ==="") {
      document.getElementById("eventtitle_invalid").innerHTML="please enter title";
      return false;
    }
  else if ((event_title.length > 150)) {
      document.getElementById("eventtitle_invalid").innerHTML="title is very long";
      return false;
    }
    else
    {

       document.getElementById("eventtitle_invalid").innerHTML="";
       return true;

   }

}
function validateeventdes()
{
  var event_discription =document.getElementById("event_discription").value;
  if (event_discription ==="") {
      document.getElementById("eventdes_invalid").innerHTML="please enter description";
      return false;
    }
else{
       document.getElementById("eventdes_invalid").innerHTML="";
       return true;

   }

}
function validateevent()
  {
    if(validateeventtitle()==true && validateeventdes()==true )
    {
    return true;
  }
    else
    {
    return false;
  }
  }
