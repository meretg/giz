<?php


return [
'main'=>'Home',
'investors'=>'Investors ',
'enterpreter '=>'Startups',
'portfilio'=>'Portfolio',
'news'=>'News &amp; Events',
'aboutus'=>'About Us',
'contactus'=>'Contact Us',
'lang'=>'language',
'investors_cover'=>' THE FIRST UPPER EGYPT ANGEL NETWORK',
'slogan'=>'BELIEVE IN IT, ACHIEVE IT',
'content1'=>'Nile Angels is the first Angel Network in Upper Egypt with focus on angel investment where it
aims to invest in local start-ups and support entrepreneurs.
Nile Angels creates investment community that helps investors enter new markets and invest
in innovative start-ups a ways from traditional business where high return on investment is
achieved , also to enhance the opportunities for entrepreneurs to be financed and accelerated
to market leadership .
Nile Angels provides more than fund, it brings consulting ,mentoring ,Connections ,knowledge
,empowerment to turn startups into a sustainable and successful business.',
'nile'=>'Nile Angels',

'servicescontent'=>' Startup is an entrepreneurial venture which is typically a newly emerged
business that aims to meet a marketplace need by developing innovative business
model around a product, service, process or a platform.Most start ups need fund in its early stage to continue and scale so one of the best ways of
funding is angel investing which is located between family and friends fund and venture capital
,also some entrepreneurs may turn to banks to get loans . ',
'consaltcontent'=>'Nile Angels is actively seeking
promising startups to scale ,it
provides consulting services
like marketing research ,feasibility
studies ,business plans ,branding
,marketing plans ,financial plans
,operations plans ,management
plans ,human resources plans
,business models.',
'consalt'=>'Consulting',
'directing'=>'Mentoring',
'directingcontent'=>'Nile Angels provides technical and
business mentoring for startups
where startups can receive
trainings ,workshops ,one on one
mentoring .',
'socity'=>'Investors and entrepreneurs Community',
'socitycontent'=>'Nile Angels works as a catalyst
between investors and entrepreneurs
where angel investors participate in
build Upper Egypt s economy and
entrepreneurs solving challenges that
face their community actively ,also
Nile Angels members are successfully
business owners and executives who
have an interest in mentoring , also
they collaborate in the due diligence
and investing in innovative high risk
startups .',
'joininvestor'=>'Apply as an Angel Investor',
'joinenter'=>'Apply as a start - up',
'client'=>'partners',
'services'=>'What We Bring',
'invest'=>'Did you invest before in startups?',
'enterpreter'=>'Startups',
'joinasenter'=>'Why Join TO NILE ANGELS As A Start-up',
'joinasenterans1'=>'Access to investors.',
'joinasenterans2'=>'Move startups to large markets through measurable growth.',
'joinasenterans3'=>'Help startups in opening new markets for their businesses.',
'joinasenterans4'=>'Help startups in building strong business models/business plans.',
'joinasenterans5'=>'Enrich startups to grow and compete in the market',
'joinasenterans6'=>'Provide startups with mentors to support them in growing and competing in the market.',
'joinasenterans7'=>'Join to investors and entrepreneurs community where they can share knowledge &make partnerships .',
'startup'=>'We are looking for innovative Start-ups that provide new products/services .
The nature of each investment will vary but Nile Angels will Evaluate Start –ups
According to the following',

'startuptitle'=>'Selection Criteria',
'COMPETITIVE'=>'COMPETITIVE ADVANTAGE',
'COMPETITIVEans'=>'the startups must have strong competitive
advantages',
'market'=>'MARKET OPPORTUNITY',
'marketans'=>'the products/services must fit the market and
havemarket Shares',
'FINANCIALS'=>'FINANCIALS',
'FINANCIALSans'=>'Startups must have prior Financial models about its
performance',
'BUSINESSMODEL'=>'BUSINESS MODEL',
'BUSINESSMODELans'=>'Startups must have scalable and strong business
model.',
'BUSINESSPLAN'=>'BUSINESS PLAN',
'BUSINESSPLANans'=>'Effective business plan with true milestones have a good
chance to get investments because it demonstrates how startups will
use Funds.',
'GROWTH'=>'GROWTH POTENTIAL',
'GROWTHans'=>'startups with high growth potential attract
investors.',
'Management'=>'Management Teams',
'Managementans'=>'we are looking for innovative entrepreneurs
where
passion
,commitment
,
high
performance
leadership
,dedication
exists.',
'Innovation'=>'Innovation',
'Innovationans'=>'Startups must have innovation part in idea or business
model.',
'frq'=>'',
'frq1'=>'',
'frq2'=>'',
'frq3'=>'',
'frq4'=>'',
'aboutustitle'=>'Who we are?',
'aboutcontent1'=>'',
'mission'=>'Mission',
'missioncontent'=>'enriching opportunities for collaboration between entrepreneurs and investors where
startups can reach to growth and prosperity and investors get high return on investment.',
'vission'=>'vission',
'vissioncontent'=>'Nile Angels aims to be the leader in the entrepreneurial community in Upper Egypt.',

'phone'=>'PHONE',
'phone2'=>'whatsUpNo',
'mail'=>'EMAIL',
'contactitle'=>'Send massage',
'name'=>'Full name',
'email'=>'Email',
'phone'=>'Phone Number',
'msg'=>'Content',
'send'=>'SEND',
'investors_cover'=>'',
'Call'=>'Call for investment',
'Filtration'=>'Filtration Process',
'Interview'=>'Interview Process',
'Pitching'=>'Pitching Process',
'Due'=>'Due Dilligence',
'Term'=>'Term sheet Negotiation',
'Funding'=>'Funding',
'Callans'=>' startups apply online in Nile Angels website and Attach their Business Plans.',
'Filtrationans'=>'Nile Angels will filter and select the promising startups',
'Interviewans'=>'Nile Angels will conduct meeting with Selected startups   where a more in depth discussion will occur on the key issues affecting the future success of the business. ',
'Pitchingans'=>'Startups will deliver a pitching to investors.',
'Dueans'=>'Nile angels will verify the statements made in  business model ,the business plan, presentation, and financial projections.  ',
'Termans'=>' Nile Angels negotiate with selected startups to   defines the structure of the investment deal .',
'Fundingans'=>' Closing the deal is only the beginning of the angel funding process ,the selected startups will get investment to grow and scale .',
'Phase'=>'Phase',
'deadline'=>'deadline',

'ApplicationProcess'=>'Application Process',
'joinasstartup'=>'Apply Now as a Startup ',
'phone3'=>'',
'selectionstrtup'=>'selection Criteria of startUps',
'Values'=>'Our Values ',




'investorrisk'=>'How do Investors reduce the risk of angel investing ?',
'Investorborrowed'=>'Can angel Investor borrowed money to invest in startups?',
'investorsget'=>'what will the investors get when invest in startups ?',
'membership'=>'How do you get membership in Nile Angels ?',
'Evaluate'=>'Who will select and Evaluate Promising Start- ups ?',

'whonileangels_title'=>'',
'investorriskans'=>'<br>risk can be reduced by diversifying across a broad portfolio of angel investments.',
'Investorborrowedans'=>'<br>No ,because angel investing is risky and investors use some of their own funds .',
'investorsgetans'=>'<br>Investor will get shares equity',
'membershipans'=>'Apply online in Nile Angel Website and Nile angels will call you to complete the
membership process .',
'Evaluateans'=>'<br>Nile angels  team and advisory members will participate in selection process and
due diligence .',
'whynileangels_title'=>'WHY JOIN NILE ANGELS ?',
'helptoknow'=>'Helping you to know the available chances of investments.',
'cantohelp'=>'Helping you find new & different opportunities in market.',
'helptochoose'=>'participating in mentoring and due diligence process
Achieving high return on investment (ROI).',
'helptochoose2'=>'Appropriate valuation of startups with reasonable terms.',
'estimatcompany'=>'Connecting with Upper Egypt Investors Where partnerships and sharing experience is evolved.',
'cantojoin'=>'Build strong relationships with Upper Egypt entrepreneurs.',
'Selection Criteria'=>'INVESTMENT CRITERIA',
'investStandardscon1'=>'We are looking for innovative Startups that provide new products/services .
The nature of each investment will vary but Nile Angels will evaluate Start –ups
according to the following',
'investoperation'=>'INVESTMENT PROCESS',
'questions'=>'FREQUENTLY ASKED QUESTIONS',
'whoinvestor'=>'Who is angel investor?',
'whonileangels_content'=>'Wealthy business owners who invest their private funds in promising startups and
who are risk takers.',
'aboutustitle'=>'Who we are?',
'aboutustitle2'=>'Our Story',
'aboutcontent1'=>'Nile Angels is the first Angel Network in Upper Egypt where it aims to invest in local startups
and support entrepreneurs.
In addition to that Nile angels provides business consulting for start-ups to assist in the
development and growth of them.
Nile Angels could mark a new era of entrepreneurship in Upper Egypt. Moreover, it does
not only serve to highlight the viability of investing in Upper Egypt, but also to inspire
investors in other cities across the country to follow their lead.',
'aboutcontent2'=>'there are dedicated e ntrepreneurs in Upper Egypt who are focusing on building their
startups and looking for support where their startups can grow and scale .
the other side ,there are investors who are searching for the opportunities to invest in
in the market but they could not find a good guide that can help to know what are the
opportunities to invest in the markets , in addition to know who will evaluate and
select the promising start ups effectively.
So Nile Angels is established to create a community that gather Upper Egypts investors
and start ups where investors can invest and
start ups can grow .
Through Nile Angels they can communicate ,interact ,collaborate ,share experience .
Nile Angels aims to bring entrepreneurs, investors, mentors, together and help them to
grow
their
Businesses
.
Nile Angels creates the network that encourages ideas owner and promising Start-ups
to get investments and make valuable connections .',
'Empowerment'=>'Empowerment',
'Difference'=>'Make a Difference',
'Partnerships'=>'Partnerships',
'Sharing'=>'Sharing Experience',
'Innovation'=>'Innovation',
'Transparency'=>'Transparency',
'Development'=>'Follow Up and Development',
'startups'=>'Startup',
'stratuphome'=>'is an entrepreneurial venture which is typically a newly emerged
business that aims to meet a marketplace need by developing innovative business
model around a product, service, process or a platform.
<br><br>
<br>
Most start ups need fund in its early stage to continue and scale so one of the best ways of
funding is angel investing which is located between family and friends fund and venture capital
,also some entrepreneurs may turn to banks to get loans .',
'investors_cover'=>'The First Upper Egypt Angel Network',
'1'=>'1',
'2'=>'2',
'3'=>'3',
'4'=>'4',
'5'=>'5',
'6'=>'6',
'7'=>'7',
'startUp'=>'Startup',

'entrepreneur'=>'StartUps',
'whats'=>'Whats app number?',
'city'=>'Your City ?',
'date'=>'Age?',
'business-fild'=>'which of the below industries would you be most interested to invest in?',
'money_invest'=>'what is the amount that you want to invest?',
'industry'=>'please check all industries you have professional experience in?',
'experience'=>'please check all your functional experiences',
'education'=>'What is your educational Background ?',
'stratUpName'=>'The Startup Name ?',
'team2'=>'If you have a team as cofounders, kindly mention their names, backgrounds, their job titles?',
'employeeNo'=>'What is the number of employees in your startup ?',
'descrip_startup'=>'Describe your Startup in brief (what is the product or service that you provide, the value added)?',
'customers'=>'Who is your customer segment ? ',
'launching'=>'When did you launch your startup ?',
'register'=>'Do you have registered Startup?',
'providing' => 'services',
'facebook'=>'If you have Facebook page kindly put the URL link here ?',
'website'=>'If you have  website kindly put the URL link here ?',
'prototype2'=>'If you have  prototype kindly put the URL link here ?',
'registeredAim'=>'Why do you register in Nile Angels ?',
'file'=>'Upload the Business Plan of your Startup ?',
'submit'=>'Submit',
'yes'=>'Yes',
'no'=>'No',
'needInvestor'=>'needInvestor',
'needConsultation'=>'needConsultation',
'needguidance'=>'needguidance',
'none'=>'none',
'executive_team'=>'Operations',
'investor_team'=>'Management',
'team'=>'team',
'news2'=>'news',
'events'=>'Events',
'details'=>'Details',
'showmore'=>'show more',
'Background'=>'What is your educational Background ?',
'date'=>'Age ?',
'experience'=>'Please check all your functional experiences.',
'background'=>'What is your educational Background ?',
'industry'=>'Please check all industries you have professional experience in?',
'invest'=>' Did you invest before in startups?',
'business-fild'=>'Which of the below industries would you be most interested to invest in? ',
'money_invest'=>'What is the amount that you want to invest?',
'finacialServices'=>'Financial Services',
'marketing'=>'Marketing',
'operations'=>'Operations',
'sales'=>'Sales',
'businessDevelopment'=>'Business Development',
'researchDevelopment'=>'Research & Development',
'publicRelations'=>'Public Relations',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',
''=>'',

];
 ?>
