
@extends("master")
@section("content")

<!-- start task2 -->
<div id="home-p" class="home-p pages-head2 text-center">
    <div class="container" style="margin-bottom:5%;">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.joinasstartup')</h1>

    </div><!--/end container-->
  </div>
<div class="container" {{ Lang::locale() === 'ar' ? ' dir=rtl': '' }}>
<!-- <div class="row">
  <div class='col-lg-12 col-xm-12'>

</div>
</div> -->


<!-- Entrepreneur -->

  <!-- <div id="Entrepreneur" class=' wow bounceIn ' style="display:none">


<form class="modal-body" id="Entrepreneur_form" enctype="multipart/form-data" role="form" method="post" action = "basic">
 <h1>@lang('messages.entrepreneur')</h1>
 @if(Session::has('flash_message'))
 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
<div class="form-group">
  <div class="row">
   <div class="col-sm-3 label_div">
    <label> @lang('arabic.name') </label></div>
  <div class="col-sm-6 classname">


   <input type="text" name="fullName" id="user2_name" required>
   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
   <input value="entrepreneur" type="hidden" name="type">
 </div>
 </div>
    <span class="text-center error" id="username2_error_message"></span>

</div>


<div class="form-group">
   <div class="row">
     <div class="col-lg-3 label_div">
   <label>@lang('arabic.phone')</label>
 </div>
   <div class="col-sm-6 classname">

   <input type="tel" id="phone2" name='phoneNo' required>
 </div>
   </div>
    <span class="text-center error" id="phone_entr_error_message"></span>
</div>






<div class="form-group">
 <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.email')</label></div>
   <div class="col-sm-6 classname">

   <input type="text" name="email" id="user_email2"  placeholder="you@example.com">
    <nav class='special'>@lang('arabic.email_nav')</nav>
  </div>
</div>
     <span class="text-center error" id="email2_error_message"></span>
</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.whats')</label></div>
   <div class="col-sm-6 classname">

   <input type="text" id="whats2" name="whatsUpNo">
 </div>
   <span class="text-center error" id="whats2_error_message"></span>
 </div>
</div>

<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.date')</label></div>
   <div class="col-sm-6 classname">
   <input type="date" id="date" name='DOB' required>

   </div>
 </div>
</div>

<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.city2')</label></div>
   <div class="col-sm-6 classname">
   <input type="text" id="city2" name="city">
 </div>
   </div>
    <span class="text-center error" id="city2_error_message"></span>
 </div>



<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.you_are')</label></div>
   <div class="col-sm-6 classname">

   <select name="education">
     <option value="">@lang('arabic.none')</option>
     <option value='graduated'> @lang('arabic.graduate')</option>
     <option value='student'> @lang('arabic.student')</option>
   </select>
    </div>
  </div>
</div>




<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.background')</label></div>
   <div class="col-sm-6 classname">
   <textarea id="background" name="background" required></textarea>
 </div>
   </div>
    <span class="text-center error" id="back_error_message"></span>
 </div>

 <div class="form-group">
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.experience')</label></div>
    <div class="col-sm-6 classname">
    <textarea id="experience" name='experienceField'></textarea>
    </div>
  </div>
 </div>

 <div class="form-group">
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.invest')</label></div>
    <div class="col-sm-6 classname">

   <select name="haveIdea" required>
     <option value='yes'> @lang('arabic.yes')</option>
     <option value='no'> @lang('arabic.no')</option>
   </select>
    </div>
  </div>
</div>



<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.suggested_business')</label></div>
   <div class="col-sm-6 classname">
    <select name="suggestedBussinessType" required>
      <option value="">@lang('arabic.none')</option>
      <option value="manufacturingBusiness">@lang('arabic.manufacturing')</option><br>
      <option value="merchandisingBusiness">@lang('arabic.merchandising')</option> <br>
      <option value="technologyBasedSolution">@lang('arabic.technology')</option><br>
      <option value="agriculturalBusiness">@lang('arabic.agriculture')</option><br>
      <option value="servicesSolutions">@lang('arabic.production')</option><br>
      <option id="other3">@lang('arabic.other')</option><br>
    </select>
  </div>
</div>
        <span class="text-center error" id="sel1_error_message"></span>
</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.stage_idea')</label></div>
 <div class=" col-sm-6 classname">
 <select name="ideaStage">
   <option value="">@lang('arabic.none')</option>
   <option value="justIdea"> @lang('arabic.idea')</option>
   <option value="Prototype"> @lang('arabic.prototype')</option>
   <option value="readyToLunching"> @lang('arabic.ready')</option>
    <option value="feasibilityStudy"> @lang('arabic.feasibilityStudy')</option>
 </select>
  </div>
 </div>
</div>


 <div class="form-group">
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.descripe_idea')</label></div>
    <div class="col-sm-6 classname">
    <textarea id="des_idea" name="ideaDescription" required></textarea>
  </div>
</div>
     <span class="text-center error" id="idea_error_message"></span>
  </div>


  <div class="form-group">
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.have_team')</label></div>
     <div class="col-sm-6 classname">
    <select name="haveTeam" required>
      <option value="">@lang('arabic.none')</option>
      <option value='yes'> @lang('arabic.yes')</option>
      <option value='no'> @lang('arabic.no')</option>
    </select>
     </div>
   </div>
</div>

<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.teamDetails')</label></div>
   <div class="col-sm-6 classname">
   <textarea name="teamDetails"> </textarea>
   </div>
 </div>
</div>

   <div class="form-group">
      <div class="row"><div class="col-lg-3 label_div">
      <label>@lang('arabic.facebook1')</label></div>
      <div class="col-sm-6 classname">
      <textarea name="facebookPage"> </textarea>
      </div>
    </div>
   </div>

   <div class="form-group">
      <div class="row"><div class="col-lg-3 label_div">
      <label>@lang('arabic.prototype')</label></div>
      <div class="col-sm-6 classname">
      <textarea name="prototype"> </textarea>
      </div>
    </div>
   </div>


  <div class="form-group">
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.looking_investor')</label></div>
     <div class="col-sm-6 classname">
    <select name="registeredAim">
      <option value="">@lang('arabic.none')</option>
      <option value='needInvestor'> @lang('arabic.needInvestor')</option>
      <option value='needConsultation'> @lang('arabic.needConsultation')</option>
    </select>
     </div>
   </div>
</div>

   <div class="form-group">
      <div class="row">
        <div class="col-lg-3 label_div">
      <label>@lang('arabic.file1')</label>
    </div>

   <div class="col-sm-6 classname">
    <input type="file" name="bussinessPlan" id="file2" accept=".txt,.doc,.docx,pdf">
   </div>

  </div>
  </div>

<div class="col-lg-12 col-sm-12 col-xs-12 eigth-button mt-5">

  <button type="submit" class="btn btn-22 justify-content-center h-100"id="submit2_btn" class="btn">@lang('arabic.submit')</button>


</div>
</form>
</div> -->

<!-- Startup -->
<h1 style="color:#0069d9;">@lang('arabic.startUp')</h1>
  <div id="startup" class=' wow bounceIn' >
<form class="modal-body" id="startup_form"  enctype="multipart/form-data" role="form" method="post" action = "basic">


<div class="form-group">
  <div class="row">
   <div class="col-lg-3 label_div">
    <label>@lang('arabic.name')<label></div>
  <div class="col-sm-6 classname">

   <input type="text" name='fullName'id="user3_name" required>
   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
   <input value="startup" type="hidden" name="type">
 </div>
 </div>
    <span class="text-center error" id="username3_error_message"></span>
</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.phone')</label></div>
   <div class="col-sm-6 classname">

   <input type="tel" id="phone3" name='phoneNo' required>
 </div>
   </div>
    <span class="text-center error" id="phone3_error_message"></span>
</div>






<div class="form-group">
 <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.email')</label></div>
   <div class="col-sm-6 classname">
<<<<<<< HEAD

   <input type="text" name='email'id="user_email3"  placeholder="you@example.com">
=======
     <div class="div_icon"></div>
   <input type="text" name='email'id="user_email3"  placeholder="you@example.com" required>
>>>>>>> a12892bd3faa5ad41c1c78ae17c60c0b372ffd50
    <nav class='special'></nav>
  </div>
</div>
     <span class="text-center error" id="email3_error_message"></span>  <!-- not required-->
</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.whats')</label></div>
   <div class="col-sm-6 classname">

   <input type="text" id="whats3" name='whatsUpNo'>
 </div>
  <span class="text-center error" id="whats3_error_message"></span>
 </div>
</div>



<!-- required -->
<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.city')</label></div>
   <div class="col-sm-6 classname">
   <input type="text" id="city3"name='city' required>
 </div>
   </div>
    <span class="text-center error" id="city3_error_message"></span>
 </div>



<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.stratUpName')</label></div>
   <div class="col-sm-6 classname">
   <input type="text" id="address" name='startUpName' required>
 </div>
</div>
     <span class="text-center error" id="address_error_message"></span>
  </div>


  <div class="form-group">   <!--hidden-->
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.education')</label></div>
     <div class="col-sm-6 classname">
     <textarea name="background" id='team_name' required> </textarea>
     </div>
   </div>
  </div>

  <div class="form-group">   <!--hidden-->
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.launching')</label></div>
     <div class="col-sm-6 classname">
       <input type="date"name="launchDate" required>
     </div>
   </div>
  </div>

  <div class="form-group">   <!--hidden-->
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.team2')</label></div>
     <div class="col-sm-6 classname">
     <textarea name="teamDetails" id='team_name' required> </textarea>
     </div>
   </div>
  </div>

  <div class="form-group" >   <!--hidden-->
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.employeeNo')</label></div>
     <div class="col-sm-6 classname">
     <textarea name="employeeNo" id='employes_name'> </textarea>
     </div>
   </div>
  </div>

  <div class="form-group">
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.descrip_startup')</label></div>
     <div class="col-sm-6 classname">
     <textarea name="ideaDescription" required> </textarea>
     </div>
   </div>
  </div>

  <div class="form-group">   <!--hidden-->
     <div class="row"><div class="col-lg-3 label_div">
     <label>@lang('arabic.customers')</label></div>
     <div class="col-sm-6 classname">
       <input type="text"name="customers">
     </div>
   </div>
  </div>






 <div class="form-group">   <!--hidden-->
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.facebook')</label></div>
    <div class="col-sm-6 classname">
    <input type="text" name="facebook" placeholder="Enter facebook URL">
    </div>
  </div>
 </div>

 <div class="form-group">   <!--hidden-->
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.prototype2')</label></div>
    <div class="col-sm-6 classname">
      <input type="text"name="prototype" placeholder="Enter prototype URL">
    </div>
  </div>
 </div>

 <div class="form-group">   <!--hidden-->
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.website')</label></div>
    <div class="col-sm-6 classname">
      <input type="text"name="website" placeholder="Enter Website URL">
    </div>
  </div>
 </div>

    <div class="form-group">
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.register')</label></div>
  <div class=" col-sm-6 classname">
 <select name="registered" required>
    <option value="">@lang('arabic.none')</option>
   <option value="yes">@lang('arabic.yes')</option>
   <option value="no">@lang('arabic.no')</option>
 </select>
    </div>
  </div>
 </div>




 <div class="form-group">
    <div class="row"><div class="col-lg-3 label_div">
    <label>@lang('arabic.registeredAim')</label></div>
  <div class=" col-sm-6 classname">

 <select name="registeredAim" required>
   <option value="">@lang('arabic.none')</option>
 <option value="needguidance">@lang('arabic.needguidance')</option>
   <option value="needInvestor">@lang('arabic.needInvestor')</option>
   <option value="needConsultation">@lang('arabic.needConsultation')</option>
 </select>
    </div>
  </div>
 </div>

 <div class="form-group">   <!--hidden-->
    <div class="row">
      <div class="col-lg-3 label_div">
    <label>@lang('arabic.file')</label>
  </div>

 <div class="col-sm-6 classname">
  <input type="file" name="bussinessPlan" id="file">
 </div>

</div>
</div>

 <div class="title-but "style="margin: 0 auto;padding: 0;text-align: center;">
 <button class="btn btn-general btn-green" id="submit3_btn" >@lang('arabic.submit')</i></button>

</div>

</form>
</div>
</div>





<!-- start alert -->


<!-- <div class="alert  center-block text-center hidden" role="alert">
  <h4 class="alert-heading">@lang('arabic.well')</h4>
  <p>@lang('arabic.thanks')</p>
</div> -->

<script>

function Entrepreneur_form()
{
  $("#Entrepreneur").css("display","block");
    $("#startup").hide();
}
function startup_form()
{
  $("#Entrepreneur").hide();
    $("#startup").css("display","block");
}
</script>


@stop
