@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Entrepreneurs</h2>
<hr>

 @if(Session::has('flash_message'))
 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="300">
  <col width="350">
  <col width="300">
  <col width="200">
  <col width="200">
    <thead>
      <tr >

        <th>Fullname</th>
        <th>Email</th>
        <th>City</th>
        <th>Status</th>
        <th>View</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr >
        @foreach($Entrepreneurs as $entrepreneur)
                  <td>{{$entrepreneur["applicant"]->fullName}} <br/></td>
                  <td>{{$entrepreneur["applicant"]->email}}</td>
                  <td>{{$entrepreneur["applicant"]->city}}</td>
                  <td>{{$entrepreneur->education}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#viewentrepreneurModal"  onclick = "entrepreneur_view('{{$entrepreneur->id}}');" id="entrepreneur_Edit" ><i class="fa fa-eye"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteentrepreneurModal"  onclick = "delete_entrepreneur('{{$entrepreneur->id}}');" id="entrepreneur_delete" >X</button></td>
              </tr>
        @endforeach

    </tbody>
  </table>
  <div class="modal fade" id="viewentrepreneurModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">View this entrepreneur</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body">
            <form class="form-horizontal">

  <div class="form-group row">
    <label class="control-label col-sm-4" >fullname :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="fullname" name="fullname"></label>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Email :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="email" name="email"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Phone number :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="phone" name=""></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Another phone no. or whatsapp no. :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="whats_phone" name=""></label>

    </div>
  </div>

  <div class="form-group row">
    <label class="control-label col-sm-4" >City :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="city" name="city"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Status :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="status" name="status"></label>

    </div>
  </div>

  <div class="form-group row">
    <label class="control-label col-sm-4" >Background :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="background" name="background"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Have an idea :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="idea" name="idea"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Business type :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="business_type" name="business_type"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Idea stage :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="idea_stage" name="idea_stage"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Description of idea :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="description" name="description"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Have a team :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="team" name="team"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-8" >Facebook-page/Demo/Prototype :</label>
    <div class="col-sm-4">
      <label class="text-primary" id="demo" name="demo"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" > Looking for an investor :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="looking_investor" name="looking_investor"></label>

    </div>
  </div>

  <div class="form-group row">
    <label class="control-label col-sm-6" >Business Plan :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="business_plan" name="business_plan"></label>

    </div>
  </div>
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="viewentrepreneur_id">
  <div class="form-group">
    <div class=" col-sm-12">
      <button  id="save_update" class="btn btn-blue pull-right"data-dismiss="modal" >Ok</button>
    </div>
  </div>
        </form>
      </div>

    </div>
  </div>

  </div>

  <div class="modal fade" id="deleteentrepreneurModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you sure ?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         <strong style="color:red;">
             You will delete all information about this entrepreneur
         </strong>
        </div>
        <div class="modal-footer">

          <form class="form-horizontal" action = "{{ route('delete_Entrepreneur') }}" method = "post">
            <input type="hidden" name="id" id="entrepreneur_id">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
            <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
          </form>


        </div>
      </div>

    </div>
  </div>
  </section>
  <script>

      function delete_entrepreneur (id)
      {
        $("#entrepreneur_id").val(id);
      }
      function entrepreneur_view(id)
  {
            $("#viewentrepreneur_id").val(id);
            $.ajax({
              url: "{{ URL::to('Entrepreneur.view') }}",
              type: "get",
              dataType: 'json',
              data: {"_token":$('#_token').val(),"id":id},
              success: function(response)
              {
                 $("#fullname").html(response["applicant"].fullName),
                 $("#email").html(response["applicant"].email),
                 $("#phone").html(response["applicant"].phoneNo),
                 $("#whats_phone").html(response["applicant"].whatsUpNo),
                $("#status").html(response.education),
                 $("#city").html(response["applicant"].city),
                 $("#background").html(response["applicant"].background),
                 $("#idea").html(response.haveIdea),
                 $("#business_type").html(response.suggestedBussinessType),
                 $("#description").html(response["applicant"].ideaDescription),
                 $("#team").html(response.haveTeam),
                 $("#demo").html(response["applicant"].demo),
                 $("#looking_investor").html(response.needInvestor),
                 $("#business_plan").html(response["applicant"].bussinessPlan),
                 $("#idea_stage").html(response.ideaStage)
                 },


              error: function () {

                  alert("error");

              }
              });
  }
    </script>
</div>
</section>
@stop
