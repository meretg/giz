@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">Partners</h2>
<button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#addpartnerModal"><i class="fa fa-plus"></i> Add partner</button>
</div>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="50">
  <col width="50">
    <thead>
      <tr >

        <th>Title</th>
        <th>Logo</th>
        <th>Bio</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Facebook</th>
        <th>Linkedin</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr >
        @foreach($partners as $partner)



                  <td>{{$partner->title}} <br/></td>
                  <td><img class="imge" src="{{asset('/PartnersLogo')}}/{{$partner->logo}}"></td>
                  <td>{{$partner->bio}}</td>
                  <td>{{$partner->email}}</td>
                  <td>{{$partner->phone}}</td>
                  <td>{{$partner->facebook}}</td>
                  <td>{{$partner->linkedin}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editpartnerModal"  onclick = "edit_partner('{{$partner->id}}');" id="partner_Edit" ><i class="fa fa-edit"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletepartnerModal"  onclick = "delete_partner('{{$partner->id}}');" id="partner_delete" >X</button></td>
              </tr>


        @endforeach

    </tbody>
  </table>

</div>
  <div class="modal fade" id="addpartnerModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Add partner</h4>

      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">

       <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('PartnerController@store') }}"  method = "post">
<div class="form-group row">
    <label class="control-label col-sm-3" >Title :</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="partener_title" name = "title"  required>
  <span id="invalid_partenertitle"></span>
</div>
</div>

<div class="form-group row">
    <label class="control-label col-sm-3">Logo :</label>
<div class="col-sm-9">
  <input class="center-block" type="file" id="logo" name="logo" onchange="checkextension();"/>
  <span id="invalid_image"></span>
</div>

</div>
  <div class="form-group row">
    <label class="control-label col-sm-3">Phone :</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="phone" name = "phone" required>
<span id="invalid_partenerphone"></span>
    </div>

</div>
<div class="form-group row">
  <label class="control-label col-sm-3">Email :</label>
  <div class="col-sm-9">
    <input type="email" class="form-control" id="email" name = "email" required>
    <span id="invalid_parteneremail"></span>
  </div>
</div>

  <div class="form-group row">
      <label class="control-label col-sm-3">Bio :</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="bio" name = "bio" required>
    <span id="invalid_partenerbio"></span>
  </div>


</div>
<div class="form-group row">
  <label class="control-label col-sm-3">Facebook:</label>
  <div class="col-sm-9">
    <input type="url" class="form-control" id="facebook" name = "facebook" required>
  </div>

</div>
<div class="form-group row">
  <label class="control-label col-sm-3">Linkedin :</label>
  <div class="col-sm-9">
    <input type="url" class="form-control" id="linkedin" name = "linkedin" required>
  </div>

</div>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" col-sm-12">
  <button type="submit" class="btn btn-blue pull-right" onclick="return validatepartener()"  id="add_save">Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="editpartnerModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit this partner</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
          <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('PartnerController@update') }}" method = "post">

            <div class="form-group row">
                <label class="control-label col-sm-3" >Title :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="old_title" name = "title"  required>
            </div>


            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">Logo :</label>
            <div class="col-sm-9">
              <input class="center-block" type="file" id="old_logo" name="logo" onchange="checkextension();"/>
              <span id="invalid_image"></span>
            </div>

            </div>
              <div class="form-group row">
                <label class="control-label col-sm-3">Phone :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="old_phone" name = "phone" required>

                </div>

            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Email :</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" id="old_email" name = "email" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Bio :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="old_bio" name = "bio" required>
              </div>

            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Facebook:</label>
              <div class="col-sm-9">
                <input type="url" class="form-control" id="old_facebook" name = "facebook" required>
              </div>


            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Linkedin :</label>
              <div class="col-sm-9">
                <input type="url" class="form-control" id="old_linkedin" name = "linkedin" required>
              </div>

            </div>

  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editpartner_id">
<div class="form-group">
  <div class=" col-sm-12">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-blue pull-right"data-dismiss="modal" >Save</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deletepartnerModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Are you sure ?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this partner
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('PartnerController@destroy') }}" method = "post">
          <input type="hidden" name="id" id="partner_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>

    function delete_partner (id)
    {
      $("#partner_id").val(id);
    }
    function edit_partner(id)
{

          $("#editpartner_id").val(id);
          $.ajax({
            url: "{{ URL::to('/partner/show') }}",
            type: "post",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#old_title").val(response.title),
               $("#old_bio").val(response.bio),
               $("#old_logo").val(response.logo),
               $("#old_email").val(response.email),
               $("#old_facebook").val(response.facebook),
               $("#old_phone").val(response.phone),
               $("#old_linkedin").val(response.linkedin)
               },


            error: function () {

                alert("error");

            }
            });
}
function checkextension() {
  var image = document.querySelector("#logo");
  // alert(image);
  if ( /\.(jpe?g|png)$/i.test(image.files[0].name) === false )
   {
   $("#invalid_image").html("Not an image !");
  }
  else {
    $("#invalid_image").html("");
  }
}
  </script>
@stop
