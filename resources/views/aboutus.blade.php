@extends("master")
@section("content")

<div id="home-p" class="home-p pages-head5 text-center">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.aboutus')</h1>

    </div>
    <!--/end container-->
</div>
<section id="about-p1">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                    <h1 style="color:#007BFF;">@lang('arabic.aboutustitle') </h1>
                    <p >@lang('arabic.aboutcontent1')</p>
          </div>
            <div class="col-md-6">

                    <h1 style="color:#007BFF;">@lang('arabic.aboutustitle2') </h1>
                  <p style="padding-bottom:20px;">@lang('arabic.aboutcontent2')</p>

            </div>
        </div>
    </div>
</section>

<section class="about-p2 bg-gradiant">
    <div class="container-fluid">
        <div class="row">

        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="about-p2-cont cl-white">
                    <img src="../img/comming-soon.jpg" class="img-fluid wow fadeInUp" data-wow-delay="0.1s" alt="...">
                </div>
            </div>
            <div class="col-md-4">
                <div class="about-p2-cont cl-white wow fadeInUp" data-wow-delay="0.3s">
                    <h5>@lang('arabic.mission')</h5>
                    <p class="cl-white">@lang('arabic.missioncontent')</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="about-p2-cont cl-white wow fadeInUp" data-wow-delay="0.5s">
                    <h5>@lang('arabic.vission')</h5>
                    <p class="cl-white">@lang('arabic.vissioncontent')</p>
                </div>
            </div>

        </div>


    </div>


</section>

<section id="project-p1" class="project-p1">
  <div class="container-fluid">
  <div class="  row"><div class="col-md-12 col-sm-6 col-xs-12 bg-matisse" ><center style="color:white;font-size:35px;font-family:bold;padding-top:2%;padding-bottom:2%;">@lang('arabic.Values') </center></div>
</div>
    <div class="  row">

          <div class="col-md-3 col-sm-6 col-xs-12 bg-starship" >
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="0.3s">
            <i class="fa fa-hand-rock-o fa-2x"></i>
            <p>@lang('arabic.Empowerment')</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 bg-fuchsia" style="background-color:#ff9966;">
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="0.6s">
            <i class="fa fa-line-chart fa-2x"></i>
            <p>@lang('arabic.Difference')</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 bg-matisse">
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="0.9s">
            <i class="fa fa-users fa-2x"></i>
            <p>@lang('arabic.Partnerships')</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 "style="background-color:#999999;">
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="1.2s">
            <i class="fa fa-share-alt fa-2x"></i>
            <p>@lang('arabic.Sharing')</p>
          </div>
        </div>
    </div>
    <div class="  row">
          <div class="col-md-4 col-sm-6 col-xs-12 bg-matisse">
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="0.3s">
            <i class="fa fa-lightbulb-o fa-2x"></i>
            <p>@lang('arabic.Innovation')</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 bg-chathams">
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="0.6s">
            <i class="fa fa-leaf fa-2x"></i>
            <p>@lang('arabic.Transparency')</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 bg-atlis">
          <div class="project-p1-cont wow fadeInUp  text-center" data-wow-delay="0.9s">
            <i class="fa fa-level-up fa-2x"></i>
            <p>@lang('arabic.Development')</p>
          </div>
        </div>

    </div>
  </div>
</section>



















@stop
