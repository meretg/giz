@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">Admins</h2>
<button class="col-sm-2  btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#addadminModal"><i class="fa fa-plus"></i> Add admin</button>
</div>

<hr>

 @if(Session::has('flash_message'))
 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div style="overflow-y:auto; overflow-x:auto;">
<table class="table table-hover ">
  <col width="300">
  <col width="350">
  <col width="200">
  <col width="200">
    <thead>
      <tr>

        <th>Username</th>
        <th>Email</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @foreach($admins as $k => $admin)



                  <td>{{$admin->username}} <br/></td>
                  <td>{{$admin->email}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editadminModal"  onclick = "edit_admin('{{$admin->id}}');" id="admin_Edit" ><i class="fa fa-edit"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteadminModal"  onclick = "delete_admin('{{$admin->id}}');" id="admin_delete" >X</button></td>
              </tr>


        @endforeach

    </tbody>
  </table>

</div>
  <div class="modal fade" id="addadminModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Add admin</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body">

       <form class="form-horizontal" enctype="multipart/form-data" action = "{{ route('add_admin') }}" method = "get">

<div class="form-group">
    <label class="control-label col" >Username</label>
<div class="col">
  <input type="text" class="form-control" id="admin_name" name = "username" onblur="validateUsername()">
  <span id="admin_nameinvalid"></span>
</div>


</div>

<div class="form-group">
    <label class="control-label col">Email </label>
<div class="col">
  <input type="email" class="form-control" id="admin_email" name = "email" onblur="validateEmail()">
  <span id="admin_emailinvalid"></span>
</div>

</div>
  <div class="form-group">
    <label class="control-label col">Password</label>
    <div class="col">
      <input type="password" class="form-control" id="admin_password" name = "password" onblur="validatepassword()">
      <span id="admin_passwordinvalid"></span>
    </div>

</div>
<div class="form-group">
  <label class="control-label col">Confirm password</label>
  <div class="col">
    <input type="password" class="form-control" id="admin_password2" name = "password2" onblur="validateconfirmpass()">
    <span id="admin_repasswordinvalid"></span>
  </div>

</div>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" col-sm-12">
  <button type="submit" class="btn btn-hover btn-blue pull-right"  id="add_save" onclick="return validate();"><i class="fa fa-save"></i> Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<div class="modal fade" id="editadminModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Edit this admin</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
          <form class="form-horizontal" action = "{{ route('edit_admin') }}" method = "get">

<div class="form-group">
  <label class="control-label col" >username</label>
  <div class="col">
    <input type="text" class="form-control" id="edit_adminusername"  name="username" required>
    <span id="admin_editusernameinvalid"></span>
  </div>
</div>
<div class="form-group">
  <label class="control-label col" >Email</label>
  <div class="col">
    <input type="email" class="form-control"   required id="edit_adminemail" name="email">
<span id="admin_editemailinvalid"></span>
  </div>
</div>

  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editadmin_id">
<div class="form-group">
  <div class=" col-sm-12">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-blue btn-hover pull-right"data-dismiss="modal" ><i class="fa fa-save"></i> Save</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deleteadminModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Are you sure ?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this admin
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ route('delete_admin') }}" method = "post">


          <input type="hidden" name="id" id="admin_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>
    var adminId;
    var editadmin_id;
    function delete_admin (id)
    {
      adminId = id;

      $("#admin_id").val(adminId);
    }
    function edit_admin(id)
{
          editadmin_id=id;
          $("#editadmin_id").val(id);
          $.ajax({
            url: "{{ URL::to('admin.view') }}",
            type: "put",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#edit_adminusername").val(response.username),
               $("#edit_adminemail").val(response.email)

               },


            error: function () {

                alert("error");

            }
            });
}
  </script>
@stop
