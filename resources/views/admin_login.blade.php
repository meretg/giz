  <!DOCTYPE html>
    <html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">

        <title>Nile Angels-Admin Login</title>
        <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}">

        <!-- global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/font-icon-style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.default.css') }}" id="theme-stylesheet">

        <!-- Core stylesheets -->
        <link rel="stylesheet" href="{{ asset('/css/login.css') }}">
    </head>

    <body>
          <section class="hero-area">
            <div class="overlay"></div>
            <div class="container">
              <div class="row">
                <div class="col-md-12 ">
                    <div class="contact-h-cont">
                      <h3 class="text-center">Nile Angels</h3><br>
                      @if(Session::has('flash_message'))
                         <div class="alert alert-info">
                             {{ Session::get('flash_message') }}
                         </div>
                      @endif
                      @if($errors->any())
                         <div class="alert alert-danger">
                             @foreach($errors->all() as $error)
                                 <p>{{ $error }}</p>
                             @endforeach
                         </div>
                      @endif
                      <form method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <label for="username">Email</label>
                          <input type="text" class="form-control" id="username" placeholder="Enter Email" name="email">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Password</label>
                          <input class="form-control" type="password" value="hunter2" id="example-password-input" name="password">
                        </div>
                        <button type="submit" class="btn btn-general btn-blue" role="button"><i fa fa-right-arrow></i>Login</button>
                      </form>
                    </div>
                </div>
              </div>
            </div>
          </section>



        <!--Global Javascript -->
        <script src="{{ asset('/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/tether.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    </body>

    </html>
