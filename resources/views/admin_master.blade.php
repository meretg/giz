<!--
author: Boostraptheme
author URL: https://boostraptheme.com
License: Creative Commons Attribution 4.0 Unported
License URL: https://creativecommons.org/licenses/by/4.0/
-->

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Nile Angels-Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <title>Nile Angels-Admin</title>
    <link rel="shortcut icon" href="{{ asset('/img/icon(15x15).png')}}">

    <!-- global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/font-icon-style.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/style.default.css')}}" id="theme-stylesheet">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{ asset('/css/ui-elements/card.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/style.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>




</head>

<body>

<!--====================================================
                         MAIN NAVBAR
======================================================-->
    <header class="header">
        <nav class="navbar navbar-expand-lg ">

            <div class="container-fluid ">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header">
                        <a href="#" class="navbar-brand">
                            <div class="brand-text brand-big hidden-lg-down"><img src="{{ asset('/img/150x50-1.png')}}" alt="Logo" class="img-fluid"></div>
                            <div class="brand-text brand-small"><img src="{{ asset('/img/32x32.png')}}" alt="Logo" class="img-fluid"></div>
                        </a>
                        <a id="toggle-btn" href="#" class="menu-btn active">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>

</div><!-- ./wrapper -->


                    </div>
                </div>
                <div class="pull-right ">
            <div id="logout_button">

            <div>
                <a href = "{{ url('admin_logout') }}" style="margin-right: 40%;
font-weight: bold;" class="btn btn-danger">Logout</a>
              </div>

        </div>
          </div>

            </div>
        </nav>
    </header>

<!--====================================================
                        PAGE CONTENT
======================================================-->
    <div class="page-content d-flex align-items-stretch">

        <!--***** SIDE NAVBAR *****-->
        <nav class="side-navbar">
            <!-- <div class="sidebar-header d-flex align-items-center">
                <div class="avatar"><img src="{{ asset('/img/avatar-1.jpg')}}" alt="..." class="img-fluid rounded-circle"></div>
                <div class="title">
                    <h1 class="h4">Steena Ben</h1>
                </div>
            </div> -->
            <!-- <hr> -->
            <!-- Sidebar Navidation Menus-->
            <ul class="list-unstyled">
                <li class="active"> <a href="{{ url('admin_dashboard') }}"><i class="fa fa-user"></i>Admins</a></li>
                <li><a href="{{ url('investors_dashboard') }}" > <i class="fa fa-money"></i>Investors</a>

                </li>
                <li> <a href="{{ url('startups_dashboard') }}"> <i class="fa fa-bar-chart"></i>Startups </a></li>
                <li><a href="{{ url('admin/portfolio') }}"> <i class="icon-grid"></i>Portfolio </a>

                </li>
                <li> <a href="{{ url('admin/partner') }}"> <i class="fa fa-handshake-o"></i>Partner </a></li>
                <li><a href="{{ url('team_dashboard') }}" > <i class="fa fa-users"></i>Team </a>

                </li>
                <li> <a href="{{ url('admin/news') }}"> <i class="fa fa-globe"></i>News </a></li>
                <li><a href="{{ url('admin/events') }}" > <i class="fa fa-calendar"></i>Events </a>

                </li>
            </ul>


        </nav>
<div >
          @yield('content')
</div>
    </div>

    <!--Global Javascript -->
    <script src="{{ asset('/js/jquery.min.js')}}"></script>
    <script src="{{ asset('/js/popper.min.js')}}"></script>
    <script src="{{ asset('/js/tether.min.js')}}"></script>
    <script src="{{ asset('/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('/js/jquery.cookie.js')}}"></script>
    <script src="{{ asset('/js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('/js/chart.min.js')}}"></script>
    <script src="{{ asset('/js/front.js')}}"></script>
    <script src="{{ asset('/js/admin.js')}}"></script>

    <!--Core Javascript -->
    <!-- <script src="js/mychart.js"></script> -->

</body>

</html>
