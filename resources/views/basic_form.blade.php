@extends("master")
@section("content")

<section >


 @if(Session::has('flash_message'))
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif



<!-- Forms -->
<div id="home-p" class="home-p pages-head2 text-center">
    <div class="container">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.investors_cover')</h1>
      <!-- <p>Discover more</p> -->
    </div><!--/end container-->
  </div>


<div class="container" >
<form class="modal-body" id="investors" role="form" method="post" action = "basic_form">
 <h1>@lang('arabic.investors')</h1>


<input value="investor" type="hidden" name="type">
<div class="form-group">
  <div class="row">
   <div class="col-lg-3 label_div">
    <label> @lang('arabic.name') </label>
  </div>
  <div class="col-sm-6 classname">

   <input type="text" name="fullName"id="user_name"  required>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
 </div>
 </div>
    <span class="text-center error" id="username_error_message" {{ Lang::locale() === 'de' ? 'right=-25px left=0': '' }}></span>

</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label> @lang('arabic.phone')</label></div>
   <div class="col-sm-6 classname">

   <input type="tel" id="phone" name='phoneNo' required>
 </div>
  </div>
    <span class="text-center error" id="phone_error_message"></span>


</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.whats')</label></div>
   <div class="col-sm-6 classname">

   <input type="tel" id="another_phone" id="whats" name='whatsUpNo'></div>
 </div>
    <span class="text-center error" id="phone2_error_message"></span>

</div>





<div class="form-group">
 <div class="row"><div class="col-lg-3 label_div">
   <label> @lang('arabic.email')</label></div>
   <div class="col-sm-6 classname">


   <input name='email' type="text" id="user_email" class="classemail" placeholder="you@example.com">

    <nav class='special'>@lang('arabic.email_nav')</nav>
  </div>
</div>
     <span class="text-center error" id="email_error_message"></span>  <!-- not required-->

</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.city2')</label></div>
   <div class="col-sm-6 classname">
   <input type="text" id="city"name='city' required>
 </div>
  </div>
    <span class="text-center error" id="city_error_message"></span>
 </div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.date')</label></div>
   <div class="col-sm-6 classname">
   <input type="date" id="date"name='DOB' required>

   </div>
 </div>
</div>



<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.education')</label></div>
   <div class="col-sm-6 classname">
  <input type="text" id="background"name='background' required>
   </div>
 </div>
</div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.experience')</label></div>
   <div class="col-sm-6 classname">
     <select name='experienceField' id="experience" required >
       <option value="">@lang('arabic.none')</option>
       <option value='finacialServices'>@lang('arabic.finacialServices')</option>
       <option value='marketing'> @lang('arabic.marketing')</option>
       <option value='operations'> @lang('arabic.operations')</option>
       <option value='sales'> @lang('arabic.sales')</option>
       <option value='businessDevelopment'> @lang('arabic.businessDevelopment')</option>
       <option value='researchDevelopment'> @lang('arabic.researchDevelopment')</option>
       <option value='publicRelations'> @lang('arabic.publicRelations')</option>
       <option value='other'> @lang('arabic.other')</option>
     </select>
   </div>
   </div.
      <span class="text-center error" id="drop3_error_message"></span>

    </div>
    <div class="form-group">
       <div class="row"><div class="col-lg-3 label_div">
       <label>@lang('arabic.industry')</label></div>
       <div class="col-sm-6 classname">

       <select name='investementIndustry' class="select1" required>
          <option value="">@lang('arabic.none')</option>
          <option value="agriculture">@lang('arabic.agriculture1')</option><br>
          <option value="education">@lang('arabic.educationinvest')</option><br>
          <option value="energy">@lang('arabic.energy')</option><br>
          <option value="e-Commerce">@lang('arabic.e-Commerce')</option><br>
          <option value="fastMovingGoods">@lang('arabic.fastMoving')</option><br>
          <option value="industrial">@lang('arabic.industrial')</option><br>
          <option value="media">@lang('arabic.media')</option><br>
          <option value="telecommunication">@lang('arabic.telecommunication')</option><br>
          <option value="software">@lang('arabic.software')</option><br>
          <option value="healthcare">@lang('arabic.healthcare')</option><br>
          <option id="other2">@lang('arabic.other')</option>

      </select>

    </div>
    </div.
       <span class="text-center error" id="drop2_error_message"></span>

     </div>


<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.invest')</label></div>
  <div class="col-sm-6 classname">

  <select name='investBefore' id="invest" required >
    <option value="">@lang('arabic.none')</option>
    <option value='yes'> @lang('arabic.yes')</option>
    <option value='no'> @lang('arabic.no')</option>
  </select>
   </div>
 </div>
 </div>








<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.business-fild')</label></div>
   <div class="col-sm-6 classname">
    <select name="investementField" required>
      <option value="">@lang('arabic.none')</option>
      <option value="manufacturingBusiness">@lang('arabic.manufacturing') </option><br>
      <option value="merchandisingBusiness">@lang('arabic.merchandising')</option> <br>
      <option value="technologyBasedSolution">@lang('arabic.technology')</option><br>
      <option value="servicesSolutions">@lang('arabic.services1')</option><br>
      <option value="agriculturalBusiness">@lang('arabic.agriculture')</option><br>
      <option value="production">@lang('arabic.production')</option><br>
      <option id="other1">@lang('arabic.other1')</option><br>
    </select>
  </div>
</div>
        <span class="text-center error" id="drop1_error_message"></span>
</div>








<div class="form-group">
   <div class="row"><div class="col-lg-3 label_div">
   <label>@lang('arabic.money_invest')</label></div>
   <div class="col-sm-6 classname">
<label></label>
<select name='investementNumber'required>
      <option value="  50000 – 100000">@lang('arabic.none')</option>
      <option value="100000 – 250000">@lang('arabic.one')</option><br>
      <option value="250000 – 500000">@lang('arabic.two')</option><br>
      <option value="500000 -750000">@lang('arabic.three')</option><br>
      <option value="750000 – 1000000">@lang('arabic.four')</option><br>
      <option value="More than 1000000">@lang('arabic.five')</option><br>

  </select>

</div>
</div>
      <span class="text-center error" id="drop3_error_message"></span>

</div>



<div class="title-but "style="margin: 0 auto;padding: 0;text-align: center;">
<button class="btn btn-general btn-green" id="submit3_btn" >@lang('arabic.submit')</i></button>

</div>
</form>
</div>
</div>
</section>
@stop
