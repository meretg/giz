@extends("master")
@section("content")


<!--====================================================
                       HOME-P
======================================================-->
    <div id="home-p" class="home-p pages-head4 text-center">
      <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.contactus')</h1>
      </div>
    </div>
    <!-- <section id="contact-p1" class=" contact-p1">
      <div class="container">
        <div class="row">
          <div class="contact-p1-cont">
            <h3>     </h3>
          </div>
          <div class="row" >
            <div class="contact-p1-cont2">
              <address class="address-details-f">

                         @lang('arabic.phone') @lang('arabic.phone2') <br>

                @lang('arabic.mail') <a href="mailto:‫‪info@nileangels.com‬‬" class="">‫‪info@nileangels.com‬‬</a>
              </address>
              <ul class="list-inline social-icon-f top-data">
                <li><a href="#" target="_empty"><i class="fa top-social fa-facebook" style="height: 25px; width:25px; line-height: 25px;"></i></a></li>
                <li><a href="#" target="_empty"><i class="fa top-social fa-twitter" style="height: 25px; width:25px; line-height: 25px;"></i></a></li>
                <li><a href="#" target="_empty"><i class="fa top-social fa-google-plus" style="height: 25px; width:25px; line-height: 25px;"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <!--====================================================
                        CONTACT-P2
    ======================================================-->
    <section id="contact-h" class="bg-parallax contact-h-bg">
      <div class="container">
        <div class="row">

          <div class="col-md-12">
            <center style="font-size:35px;">@lang('arabic.contactitle')  </center>
            <div class="contact-h-cont">

              <form  class="col-md-12">
                <div class="row">
                  <label class="col-md-3" for="name" style="font-size:25px;color:gray;">@lang('arabic.name'):</label>
                 <input type="text" class="col-md-7 form-control" id="name" aria-describedby="nameHelp" placeholder="">
               </div><br>
                <div class="row">
                  <label  class="col-md-3" for="exampleInputEmail1" style="font-size:25px;color:gray;">@lang('arabic.email'):</label>
                  <input type="email" class=" col-md-7  form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                </div><br>
                <div class="row">
                  <label class="col-md-3"  for="subject" style="font-size:25px;color:gray;">@lang('arabic.phone'):</label>
                  <input type="text" class="col-md-7  form-control" id="subject" aria-describedby="subjectHelp" placeholder="">
                </div><br>
                <div class="row">
                  <label  class="col-md-3" for="message" style="font-size:25px;color:gray;" >@lang('arabic.msg'):</label>
                  <textarea class=" col-md-7 form-control" id="message" rows="3"></textarea>
                </div><br>
              <center>  <button class="btn btn-general btn-white" role="button"><i fa fa-right-arrow></i> @lang('arabic.send')</button></center>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>


@stop
