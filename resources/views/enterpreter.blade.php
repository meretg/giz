@extends("master")
@section("content")


<div id="home-p" class="home-p pages-head1 text-center">
  <div class="container">
    <h1 class="wow fadeInUp" data-wow-delay="0.1s"  style="Font-size:106px;">@lang('arabic.startUp') </h1>

  </div><!--/end container-->
</div>
<section id="about" class="career-p1 about">

  <div class="container">
    <div class="row">
      <div class="col-md-6" >
        <div class="career-p1-himg" >
          <img src="../img/image-3.jpg" class="img-fluid wow fadeInUp" data-wow-delay="0.1s" alt="">
        </div>
      </div>
      <div class="col-md-6">
        <div class="career-p1-desc">
          <h4>@lang('arabic.joinasenter')</h4>
          <div class="heading-border-light"></div>

          <ul>
            <li class="col-sm-12 "><i class="fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans1')</li>
            <li class="col-sm-12 "><i class=" fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans2')</li><br>
            <li class="col-sm-12 " ><i class="fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans3')</li><br>
            <li class="col-sm-12 " ><i class="fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans4')</li><br>
            <li class="col-sm-12 " ><i class="fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans5')</li><br>
            <li class="col-sm-12 " ><i class="fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans6')</li><br><br>
            <li class="col-sm-12 "><i class="fa fa-arrow-circle-o-right"></i> @lang('arabic.joinasenterans7')</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="business-growth-p1" class="business-growth-p1 bg-gray">
  <div class="container">
    <div class="row title-bar">
      <div class="col-md-12">
        <h1 class="wow fadeInUp">@lang('arabic.selectionstrtup')</h1>
        <div class="heading-border"></div>

      </div>
    </div>

    <div class="row wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item">
            <div class="service-item-icon"> <i class="fa fa-trophy fa-3x"></i>
              </div>
              <div class="service-item-title">
                  <h3>@lang('arabic.COMPETITIVE')</h3>
              </div>

              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.COMPETITIVEans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item">
            <div class="service-item-icon"> <i class="fa fa-cogs fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3>@lang('arabic.market')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.marketans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item">
            <div class="service-item-icon"> <i class="fa fa-bar-chart fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3 >@lang('arabic.BUSINESSMODEL')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.BUSINESSMODELans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item right-bord">
            <div class="service-item-icon"> <i class="fa fa-tasks fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3 >@lang('arabic.BUSINESSPLAN')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.BUSINESSPLANans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item right-bord">
            <div class="service-item-icon"> <i class="fa fa-money fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3 >@lang('arabic.FINANCIALS')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.FINANCIALSans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item right-bord">
            <div class="service-item-icon"> <i class="fa fa-line-chart fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3 >@lang('arabic.GROWTH')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.GROWTHans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item right-bord">
            <div class="service-item-icon"> <i class="fa fa-users fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3 >@lang('arabic.Management')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.Managementans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
      <div class="col-md-3 col-sm-6 service-padding">
          <div class="service-item right-bord">
            <div class="service-item-icon"> <i class="fa fa-lightbulb-o fa-3x"></i>
                            </div>
              <div class="service-item-title">
                  <h3  >@lang('arabic.Innovation')</h3>
              </div>
              <div class="service-item-desc">
                  <p class="pfont">@lang('arabic.Innovationans')</p>
                  <div class="content-title-underline-light"></div>
              </div>
          </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="what-we-do bg-gradiant financial-p5">
  <div class="container">
    <div class="row title-bar">
      <div class="col-md-12">
        <h1 class="wow fadeInUp cl-white">@lang('arabic.investoperation')</h1>
        <div class="heading-border bg-white"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">

            <img src="../img/Artboard 1(1).png" style="width:200px">
              <h4 style="color:white;">@lang('arabic.Call')</h4>
              <br>
              <p style="font-size:20px;">@lang('arabic.Callans')</p>

            </div>
          </div>
          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">

              <img src="../img/Artboard 2.png" style="width:200px">
              <h4 style="color:white;">@lang('arabic.Filtration')</h4>
              <br>
              <p style="font-size:20px;">@lang('arabic.Filtrationans')</p>

            </div>
          </div>

          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">

            <img src="../img/Artboard 3.png" style="width:200px">
            <h4 style="color:white;">@lang('arabic.Pitching')</h4>
              <br>
                <p style="font-size:20px;">@lang('arabic.Pitchingans')</p>


            </div>

          </div>
          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">

            <img src="../img/Artboard 4.png" style="width:200px">
            <h4 style="color:white;">@lang('arabic.Interview')</h4>
              <br>
                <p style="font-size:20px;">@lang('arabic.Interviewans')</p>

            </div>
          </div>

          <div class="col-md-4  col-sm-6" >
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
            <img src="../img/Artboard 5.png" style="width:200px">
              <h4 style="color:white;">@lang('arabic.Due')</h4>
              <br>
                <p style="font-size:20px;">@lang('arabic.Dueans')</p>
            </div>
          </div>
          <div class="col-md-4  col-sm-6" >
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
            <img src="../img/Artboard 6.png" style="width:200px">
            <h4 style="color:white;">@lang('arabic.Term')</h4>
              <br>
              <p style="font-size:20px;">@lang('arabic.Termans')</p>
            </div>

        </div>
        <div class="col-md-4  col-sm-6" >

        </div>
        <div class="col-md-4  col-sm-6" >
          <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
          <img src="../img/Artboard 7.png" style="width:200px">
            <h4 style="color:white;">@lang('arabic.Funding')</h4>
            <br>
            <p style="font-size:20px;">@lang('arabic.Fundingans')</p>
          </div>
        </div>


        </div>
      </div>
    </div>
    <br><br>


</section> -->


<center style="padding-top:25px; background-color:#F0F0F0;" >
  <h1 class="wow fadeInUp" style="padding-bottom:30px; color:#007BB3"> @lang('arabic.ApplicationProcess')</h1>
    <div class="heading-border"></div>

  <table class="table  table-hover "style="width:50% ; ">
    <thead>
      <tr>
        <th style="text-align:center; color:#007BB3">@lang('arabic.Phase')</th>
        <th style="text-align:center; color:#007BB3">@lang('arabic.deadline')</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;">@lang('arabic.1')-@lang('arabic.Call')</td>
        <td>1</td>
      </tr>
      <tr>
        <td style="text-align:center;">@lang('arabic.2')-@lang('arabic.Filtration')</td>
        <td>2</td>
      </tr>
      <tr>
        <td style="text-align:center;">@lang('arabic.3')-@lang('arabic.Interview')</td>
        <td>3</td>
      </tr>
      <tr>
        <td style="text-align:center;">@lang('arabic.4')-@lang('arabic.Pitching')</td>
        <td>4</td>
      </tr>
      <tr>
        <td style="text-align:center;">@lang('arabic.5')-@lang('arabic.Due')</td>
        <td>5</td>
      </tr>
      <tr>
        <td style="text-align:center;" >@lang('arabic.6')-@lang('arabic.Term')</td>
        <td>6</td>
      </tr>
      <tr>
        <td style="text-align:center;">@lang('arabic.7')-@lang('arabic.Funding')</td>
        <td>7</td>
      </tr>

    </tbody>
    <br>
  </table>
  <br><br>
</center>
<div class="row justify-content-center align-items-center shadow">
      <a href="{{ url('/entrepreneur') }}" class="btn btn-general btn-green shadow" style="width:auto;" role="button">@lang('arabic.joinasstartup')</a>
</div>
          </div>
        </div><!--/container -->
      </div>
    </div>




  @stop
