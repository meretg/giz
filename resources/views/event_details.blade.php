@extends("master")
@section("content")
<div id="home-p" class="home-p pages-head1 text-center">
      <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.details')</h1>

      </div><!--/end container-->
    </div>

<!--====================================================
                      event DETAILS
======================================================-->
    <section id="single-news-p1" class="single-news-p1">
      <div class="container">
        <div class="row">

          <!-- left news details -->
          <div class="col-md-8">
            <div class="single-news-p1-cont">
              <div class="single-news-img">
                <img src="{{asset('/events')}}/{{$event->pic[0]->pic}}" alt="" class="img-fluide">
              </div>
              <div class="single-news-desc">
                  <h3 style="color:#3C9CF3">{{$event->title}}</h3>

                <hr>
                <div class="">
                    <p style="font-weight:400%;font-size:25px;">{{$event->description}}</p>
                  </footer>
                  </blockquote>
                </div>

              </div>
            </div>

          </div>

          <!-- Right news details -->

        </div>
      </div>
    </section>
    @stop
