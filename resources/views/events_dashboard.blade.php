@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">Events</h2>
<button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#addeventModal"><i class="fa fa-plus"></i> Add event</button>
</div>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="150">
  <col width="50">
  <col width="50">
  <thead>
       <tr >
         <th>Title</th>
         <th>Pictures</th>
         <th>Description</th>
         <th>Link</th>
         <th>From</th>
         <th>To</th>
         <th>Registration Deadline</th>
         <th>Edit</th>
         <th>Delete</th>
       </tr>
     </thead>
     <tbody>

         @foreach($objects as $event)
         <tr >
                   <td>{{$event->title}} <br/></td>
                   <td>
                   @foreach($event->pictures as $picture)
                   <img class="imge" src="{{asset('/events')}}/{{$picture->pic}}" style="width:50px; height:50px;">
                   @endforeach
                   </td>
                   <td>{{$event->description}}</td>
                   <td>{{$event->link}}</td>
                   <td>{{$event->from}}</td>
                   <td>{{$event->to}}</td>
                   <td>{{$event->registrationDeadline}}</td>
                   <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editeventModal"  onclick = "edit_event('{{$event->id}}');" id="event_Edit" ><i class="fa fa-edit"></i></button></td>
                 <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteeventModal"  onclick = "delete_event('{{$event->id}}');" id="event_delete" >X</button></td>
                 </tr>
          @endforeach

              </tbody>


  </table>

</div>
  <div class="modal fade" id="addeventModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Add event</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body">

       <form class="form-horizontal"  enctype="multipart/form-data" action = "{{ action('EventController@store') }}" method = "post">

<div class="form-group row">
    <label class="control-label col-sm-3" >Title :</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="event_title" required name = "title" onblur="validateeventtitle()">
  <span id="eventtitle_invalid"></span>
</div>


</div>


<div class="form-group row">
    <label class="control-label col-sm-3">Pictures :</label>

<div class="col-sm-9">
  <input class="center-block" type="file" id="image" name="pictures[]" multiple="multiple" onchange="checkextension();"/>
  <span id="invalid_image"></span>
</div>

</div>

<div class="form-group row">
    <label class="control-label col-sm-3">Description:</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="event_discription" name = "discription" required onblur="validateeventdes()">
<span id="eventdes_invalid"></span>
</div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-3">Link :</label>
  <div class="col-sm-9">
    <input type="url" class="form-control" id="link" name = "link" required onblur="">
    <span id="admin_repasswordinvalid"></span>
  </div>

</div>


<div class="form-group row">
  <label class="control-label col-sm-3">From :</label>
  <div class="col-sm-9">
    <input type="date" class="form-control" class="from_date" name = "from_date" required onblur="">

    <span id="admin_repasswordinvalid"></span>
  </div>

</div>


<div class="form-group row">
  <label class="control-label col-sm-3">To :</label>
  <div class="col-sm-9">
    <input type="date" class="form-control" class="to_date" name = "to_date" required onblur="">
    <span id="admin_repasswordinvalid"></span>
  </div>

</div>

<div class="form-group row">
  <label class="control-label col-sm-3">Registration Deadline:</label>
  <div class="col-sm-9">
    <input type="date" class="form-control" class="deadline" name = "deadline" required onblur="">
    <span id="admin_repasswordinvalid"></span>
  </div>


</div>

<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group row">
<div class=" col-sm-12">
  <button type="submit" class="btn btn-blue pull-right" onclick="return validateevent()" id="add_save" >Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>


<div class="modal fade" id="editeventModal" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit this event</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
        <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('EventController@update') }}" method = "post">

 <div class="form-group row">
     <label class="control-label col-sm-3" >Title :</label>
 <div class="col-sm-9">
   <input type="text" class="form-control" id="oldtitle" required name = "title" onblur="">
   <span id="title_invalid"></span>
 </div>


 </div>


 <div class="form-group row">
     <label class="control-label col-sm-3">Pictures :</label>


 <div class="col-sm-9">
   <input class="center-block" type="file" id="image" name="pictures" multiple="multiple" onchange="checkextension();"/>
   <span id="invalid_image"></span>
 </div>
 </div>


 <div class="form-group row">
     <label class="control-label col-sm-3">Description:</label>
 <div class="col-sm-9">
   <input type="text" class="form-control" id="olddiscription" name = "discription" required onblur="">

 </div>
 </div>
 <div class="form-group row">
   <label class="control-label col-sm-3">Link :</label>
   <div class="col-sm-9">
     <input type="url" class="form-control" id="oldlink" name = "link" required onblur="">

   </div>

 </div>


 <div class="form-group row">
   <label class="control-label col-sm-3">From :</label>
   <div class="col-sm-9">
     <input type="date" class="form-control" id="from_date" class="from_date" name ="from_date" required onblur="">
   </div>

 </div>

 <div class="form-group row">
   <label class="control-label col-sm-3">To :</label>
   <div class="col-sm-9">
     <input type="date" class="form-control" id="to_date" class="to_date" name ="to_date" required onblur="">
</div>
</div>


 <div class="form-group row">
   <label class="control-label col-sm-3">Registration Deadline:</label>
   <div class="col-sm-9">
     <input type="date" class="form-control" id="deadline" class="deadline" name ="deadline" required onblur="">

   </div>

 </div>
<input type="hidden" name="id" id="editevent_id">
 <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
 <div class="form-group">
 <div class=" col-sm-12">
   <button type="submit" class="btn btn-blue pull-right"  id="add_save" >Save</button>
 </div>
 </div>
 </form>
    </div>

  </div>
</div>

</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="deleteeventModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Are you sure ?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this event
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('EventController@destroy') }}" method = "post">


          <input type="hidden" name="id" id="event_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>

    function delete_event(id)
    {

      $("#event_id").val(id);
    }
    function edit_event(id)
{
          $("#editevent_id").val(id);
          $.ajax({
            url: "{{ URL::to('/events/show') }}",
            type: "post",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#oldtitle").val(response.title),
               $("#olddiscription").val(response.description),
               $("#oldlink").val(response.link),
               $("#from_date").val(response.fromdate),
               $("#to_date").val(response.todate),
               $("#deadline").val(response.deadline)

               },


            error: function () {

                alert("error");

            }
            });
}
function checkextension() {
  var images = document.getElementById("image");
  var newimages = document.getElementById("newimage");
if ('files' in images) {
    if (images.files.length == 0) {
        $("#invalid_image").html("Select one or more images.");
    } else {
        for (var i = 0; i < images.files.length; i++) {
            // var file = x.files[i];
              if ( /\.(jpe?g|png)$/i.test(images.files[i].name) === false )
            {
                $("#invalid_image").html("one of files not an image !")
            }

        }
    }
}
if ('files' in newimages) {
    if (newimages.files.length == 0) {
        $("#newinvalid_image").html("Select one or more images.");
    } else {
        for (var i = 0; i < newimages.files.length; i++) {
            // var file = x.files[i];
              if ( /\.(jpe?g|png)$/i.test(newimages.files[i].name) === false )
            {
                $("#newinvalid_image").html("one of files not an image !")
            }

        }
    }
}

}
$('.to_date').datepicker({ dateFormat: 'yy-mm-dd' });
$('.deadline').datepicker({ dateFormat: 'yy-mm-dd' });
$('.from_date').datepicker({ dateFormat: 'yy-mm-dd' });

  </script>
@stop
