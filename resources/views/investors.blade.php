@extends("master")
@section("content")
<div class="bg-gray">
<div id="home-p" class="home-p pages-head2 text-center  ">
    <div class="container" style="margin-bottom:5%;">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.investors_cover')</h1>
    </div><!--/end container-->
  </div>
  <div class="container" {{ Lang::locale() === 'ar' ? ' dir=rtl': '' }}>


    <div class="container business-growth-p1">

        <div class="row title-bar">
          <div class="col-md-12">
            <h3 class="wow fadeInUp">@lang('arabic.whynileangels_title')</h3>
            <div class="heading-border"></div>
          </div>
        </div>

      <div class="row wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">

            <div class="col-md-4 col-sm-6 service-padding">
                <div class="service-item ">
                    <div class="service-item-icon"> <i class="fa fa-question-circle fa-3x"></i>
                    </div>
                    <div class="service-item-title">
                        <h3 class="pfont">@lang('arabic.helptoknow')</h3>
                    </div>
                    <div class="service-item-desc">

                        <div class="content-title-underline-light"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 service-padding">
                <div class="service-item">
                    <div class="service-item-icon"> <i class="fa fa-handshake-o fa-3x"></i>
                    </div>
                    <div class="service-item-title">
                        <h3 class="pfont">@lang('arabic.cantohelp')</h3>
                    </div>
                    <div class="service-item-desc">

                        <div class="content-title-underline-light"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 service-padding">
                <div class="service-item right-bord">
                    <div class="service-item-icon"> <i class="fa fa-search fa-3x"></i>
                    </div>
                    <div class="service-item-title">
                        <h3 class="pfont">@lang('arabic.helptochoose')</h3>
                    </div>
                    <div class="service-item-desc">

                        <div class="content-title-underline-light"></div>
                    </div>
                </div>
            </div>
          </div>
    <div class="row wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.5">

          <div class="col-md-4 col-sm-6 service-padding">
              <div class="service-item">
                  <div class="service-item-icon"> <i class="fa fa-bar-chart fa-3x"></i>
                  </div>
                  <div class="service-item-title">
                      <h3 class="pfont">@lang('arabic.helptochoose2')</h3>
                  </div>
                  <div class="service-item-desc">

                      <div class="content-title-underline-light"></div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 service-padding">
              <div class="service-item">
                  <div class="service-item-icon"> <i class="fa fa-table fa-3x"></i>
                  </div>
                  <div class="service-item-title">
                      <h3 class="pfont">@lang('arabic.estimatcompany')</h3>
                  </div>
                  <div class="service-item-desc">

                      <div class="content-title-underline-light"></div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-6 service-padding">
              <div class="service-item right-bord">
                  <div class="service-item-icon"> <i class=" 	fa fa-link fa-3x"></i>
                  </div>
                  <div class="service-item-title">
                      <h3 class="pfont">@lang('arabic.cantojoin')</h3>
                  </div>
                  <div class="service-item-desc">

                      <div class="content-title-underline-light"></div>
                  </div>
              </div>
          </div>
        </div>

      </div>

  </div>

  <section id="about" class="career-p1 about">
   <div class="container">
     <div class="row title-bar">
       <div class="col-md-12">
         <h1 class="wow fadeInUp">@lang('arabic.startuptitle')</h1>
         <div class="heading-border"></div>
         </div>
     </div>
   </div>
   <div class="container">
     <div class="row">      <div class="col-md-6">
         <div class="career-p1-desc">
           <h4>@lang('arabic.investStandardscon1')</h4>
           <div class="heading-border-light"></div>
             <ul>
               <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right" ></i>@lang('arabic.COMPETITIVE'):</li>
             <p>@lang('arabic.COMPETITIVEans')</p>
               <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right "></i> @lang('arabic.market'):</li>
             <p>@lang('arabic.marketans')</p>
             <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right" ></i>@lang('arabic.FINANCIALS'):</li>
            <p>@lang('arabic.FINANCIALSans')</p>
             <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right "></i> @lang('arabic.BUSINESSMODEL'):</li>
            <p>@lang('arabic.BUSINESSMODELans')</p>
           <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right" ></i>@lang('arabic.BUSINESSPLAN'):</li>
           <p>@lang('arabic.BUSINESSPLANans')</p>
           <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right "></i> @lang('arabic.GROWTH'):</li>
           <p>@lang('arabic.GROWTHans')</p>
         <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right "></i> @lang('arabic.Management'):</li>
         <p>@lang('arabic.Managementans')</p>
       <li style="color:#2196f3
  ;"><i class="fa fa-arrow-circle-o-right "></i> @lang('arabic.Innovation'):</li>
         <p>@lang('arabic.Innovationans')</p>
           </ul>
         </div>
       </div>
       <div class="col-md-6" >          <img src="../img/map.PNG" class="img-fluid wow fadeInUp" data-wow-delay="0.1s" alt=""style="border:1px solid #A9D9FF
  ;">      </div>
     </div>
   </div>
  </section>






<section class="what-we-do bg-gradiant financial-p5">
  <div class="container">
    <div class="row title-bar">
      <div class="col-md-12">
        <h1 class="wow fadeInUp cl-white">@lang('arabic.investoperation')</h1>
        <div class="heading-border bg-white"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
              <!-- fa fa-spinner -->
            <img src="../img/Artboard 1(1).png" style="width:200px">
              <h3>@lang('arabic.Call')</h3>
              <br>
              <p style="font-size:20px;">@lang('arabic.Callans')</p>

            </div>
          </div>
          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
              <!-- fa fa-bullseye -->
              <img src="../img/Artboard 2.png" style="width:200px">
              <h3>@lang('arabic.Filtration')</h3>
              <br>
              <p style="font-size:20px;">@lang('arabic.Filtrationans')</p>

            </div>
          </div>

          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
              <!-- fas fa-user-tie -->
            <img src="../img/Artboard 3.png" style="width:200px">
              <h3>@lang('arabic.Pitching')</h3>
              <br>
                <p style="font-size:20px;">@lang('arabic.Pitchingans')</p>


            </div>

          </div>
          <div class="col-md-4  col-sm-6">
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
              <!-- fa fa-pie-chart -->
            <img src="../img/Artboard 4.png" style="width:200px">
              <h3 >@lang('arabic.Interview')</h3>
              <br>
                <p style="font-size:20px;">@lang('arabic.Interviewans')</p>

            </div>
          </div>

          <div class="col-md-4  col-sm-6" >
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
            <img src="../img/Artboard 5.png" style="width:200px">
              <h3>@lang('arabic.Due')</h3>
              <br>
                <p style="font-size:20px;">@lang('arabic.Dueans')</p>
            </div>
          </div>
          <div class="col-md-4  col-sm-6" >
            <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
            <img src="../img/Artboard 6.png" style="width:200px">
              <h3 >@lang('arabic.Term')</h3>
              <br>
              <p style="font-size:20px;">@lang('arabic.Termans')</p>
            </div>

        </div>
        <div class="col-md-4  col-sm-6" >

        </div>
        <div class="col-md-4  col-sm-6" >
          <div class="what-we-desc wow fadeInUp" data-wow-delay="0.1s">
          <img src="../img/Artboard 7.png" style="width:200px">
            <h3>@lang('arabic.Funding')</h3>
            <br>
            <p style="font-size:20px;">@lang('arabic.Fundingans')</p>
          </div>
        </div>


        </div>
      </div>
    </div>
    <br><br>


</section>
<section id="price">
    <div class="container">
      <h4>@lang('arabic.questions') : </h4>

      <div class="heading-border-light"></div>
        <div class="row" >
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricing-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>


                    <div class="pricingHeader">
                        <h3 class="title">@lang('arabic.whoinvestor')</h3>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>@lang('arabic.whonileangels_content')</li>
                            <br><br><br>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable active">
                    <div class="pricing-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>


                    <div class="pricingHeader">
                        <h3 class="title">@lang('arabic.investorrisk')</h3>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>@lang('arabic.investorriskans')</li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricing-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>

                    <div class="pricingHeader">
                        <h3 class="title">@lang('arabic.Investorborrowed')</h3>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>@lang('arabic.Investorborrowedans')</li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricing-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>


                    <div class="pricingHeader">
                        <h3 class="title">@lang('arabic.investorsget')</h3>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>@lang('arabic.investorsgetans')</li>
                            <br><br>
                        </ul>
                    </div>

                </div>
            </div>
            <br>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable active">
                    <div class="pricing-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>


                    <div class="pricingHeader">
                        <h3 class="title">@lang('arabic.membership')</h3>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>@lang('arabic.membershipans')</li>

                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricing-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>

                    <div class="pricingHeader">
                        <h3 class="title">@lang('arabic.Evaluate')</h3>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>@lang('arabic.Evaluateans')</li>

                        </ul>
                    </div>

                </div>
            </div>
         </div>
    </div>
</section>
<div class="row justify-content-center align-items-center shadow" style="padding-top:50px;padding-bottom:50px;">
      <a href="{{ url('/investor') }}" class="btn btn-general btn-green shadow" style="width:auto;" role="button">@lang('arabic.joininvestor')</a>

      </div>
</div>


@stop
