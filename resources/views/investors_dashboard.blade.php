@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Investors</h2>
<hr>

 @if(Session::has('flash_message'))
 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table table-hover table-responsive">
  <col width="300">
  <col width="350">
  <col width="300">
  <col width="200">
  <col width="200">
    <thead>
      <tr>
        <th >Fullname</th>
        <th >Email</th>
        <th >phone</th>
        <th >City</th>
        <th >View</th>
        <th >Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @foreach($investors as $investor)
                  <td >{{$investor["applicant"]->fullName}} <br/></td>
                  <td>{{$investor["applicant"]->email}}</td>
                  <td>{{$investor["applicant"]->phoneNo}}</td>
                  <td>{{$investor["applicant"]->city}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#viewinvestorModal"  onclick = "investor_view('{{$investor->id}}');" id="investor_Edit" ><i class="fa fa-eye"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteinvestorModal"  onclick = "delete_investor('{{$investor->id}}');" id="investor_delete" >X</button></td>
              </tr>
        @endforeach

    </tbody>
  </table>
</div>
  <div class="modal fade" id="viewinvestorModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">View this investor</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">

  <div class="form-group row">
    <label class="control-label col-sm-3" >Fullname :</label>
    <div class="col-sm-9">
      <span class="text-primary" id="fullname" name="fullame"></span>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3" >Email :</label>
    <div class="col-sm-9">
      <span class="text-primary" id="email" name="email"></span>

    </div>
  </div>


  <div class="form-group row">
    <label class="control-label col-sm-4" >Phone number : </label>
    <div class="col-sm-8">
      <span class="text-primary" id="phone" name=""></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-8" >Another phone no. or whatsapp no. : </label>
    <div class="col-sm-4">
      <span class="text-primary" id="whats_phone" name=""></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3" >Age : </label>
    <div class="col-sm-9">
      <span class="text-primary" id="age" name="age"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3" >City :</label>
    <div class="col-sm-9">
      <span class="text-primary" id="city" name="city"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Background :</label>
    <div class="col-sm-8">
      <span class="text-primary" id="background" name="background"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Experience :</label>
    <div class="col-sm-8">
      <span class="text-primary" id="experience" name="experience"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Invest before :</label>
    <div class="col-sm-8">
      <span class="text-primary" id="invest_before" name="invest_before"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-9" >Type of industry or the name of business :</label>
    <div class="col-sm-3">
      <span class="text-primary" id="industry_business" name="industry_business"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-8" >Business field want to invest :</label>
    <div class="col-sm-2">
      <span class="text-primary" id="business_field" name="business_field"></span>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-8" >Industry looking to invest in :</label>
    <div class="col-sm-2">
      <span class="text-primary" id="industry_to_invest" name="industry_to_invest"></span>

    </div>
  </div>

<div class="form-group row">
  <label class="control-label col-sm-7" > Number will invest :</label>
  <div class="col-sm-5">
    <span class="text-primary" id="number_will_invest" name="number_will_invest"></span>

  </div>
</div>
</div>
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="viewinvestor_id">
  <div class="form-group">
    <div class=" col-sm-12">
      <button  id="save_update" class="btn btn-blue pull-right"data-dismiss="modal" >Ok</button>
    </div>
  </div>
        </form>
      </div>

    </div>
  </div>

  <!-- </div> -->

  <div class="modal fade" id="deleteinvestorModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you sure ?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>


        </div>
        <div class="modal-body">
         <strong style="color:red;">
             You will delete all information about this investor
         </strong>
        </div>
        <div class="modal-footer">

          <form class="form-horizontal" action = "{{ route('delete_investor') }}" method = "post">
            <input type="hidden" name="id" id="investor_id">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
            <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
          </form>


        </div>
      </div>

    </div>
  </div>
</div>
  </section>
  <script>

      function delete_investor (id)
      {
        $("#investor_id").val(id);
      }
      function investor_view(id)
  {
            $("#viewinvestor_id").val(id);

            $.ajax({
              url: "{{ URL::to('investors.view') }}",
              type: "get",
              dataType: 'json',
              data: {"_token":$('#_token').val(),"id":id},
              success: function(response)
              {
                $("#fullname").html(response["applicant"].fullName),
                $("#email").html(response["applicant"].email),
                $("#phone").html(response["applicant"].phoneNo),
                $("#whats_phone").html(response["applicant"].whatsUpNo),
                $("#age").html(response["applicant"].age),
                $("#city").html(response["applicant"].city),
                $("#background").html(response["applicant"].background),
                $("#experience").html(response["applicant"].experienceField),
                 $("#invest_before").html(response.investBefore),
                 $("#industry_business").html(response.previousInvestement),
                 $("#business_field").html(response.investementField),
                 $("#industry_to_invest").html(response.investementIndustry),
                 $("#number_will_invest").html(response.investementNumber)

                 },


              error: function () {

                  alert("error");

              }
              });
  }
    </script>
</div>
</section>
@stop
