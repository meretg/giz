<!DOCTYPE html>
<html {{ Lang::locale() === 'ar' ? ' dir=rtl display=inline-block': '' }}>

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nile Angels</title>
    <link rel="shortcut icon" href="{{ asset('/img/icon(15x15).png')}}">

    <!-- Global Stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/basic_form.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/careers.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/contact.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/about.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/services.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/news.css')}}">


  </head>

  <body id="page-top" >

<!--====================================================
                         HEADER
======================================================-->

    <header>

      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-light" id="mainNav" data-toggle="affix">
        <div class="container">
          <a class="navbar-brand smooth-scroll" href="{{ url('/') }}">
            <img src="{{ asset('/img/nile-angels(250x50).png')}}" style="width:200px; " alt="logo">
          </a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                  <li class="nav-item" ><a  href="{{ url('/') }}">@lang('arabic.main')</a></li>
                  <li class="nav-item" ><a  href="{{ url('/investors') }}">@lang('arabic.investors')</a></li>
                  <li class="nav-item" ><a  href="{{ url('/enter') }}">@lang('arabic.startUp')</a></li>
                  <li class="nav-item" ><a  href="{{ url('/portfolio') }}">@lang('arabic.portfilio')</a></li>
                  <li class="nav-item" ><a  href="{{ url('/news') }}">@lang('arabic.news') </a></li>
                  <li class="nav-item" ><a  href="{{ url('/Team') }}">@lang('arabic.team') </a></li>
                  <li class="nav-item" ><a  href="{{ url('/aboutus') }}">@lang('arabic.aboutus')</a></li>
                  <li class="nav-item" ><a  href="{{ url('/contactus') }}">@lang('arabic.contactus')</a></li>


                <li>
                  <div class="dropdown">
                     <button style="margin-top: 15px;font-size: 17px;padding-left: 7px;" class="btn btn-primary dropdown-toggle colo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       @lang('arabic.lang') <i class="fa fa-angle-down"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                       <a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL('en') }}" {{Lang::locale()==='en' ? 'dir=ltr' : ''}}>English</a>
                       <a class="dropdown-item" id="change" href="{{ LaravelLocalization::getLocalizedURL('ar') }}" {{Lang::locale()==='ar' ? 'dir=rtl display=inline-block' : ''}}>Arabic</a>

                     </div>
                   </div>
                </li>


            </ul>
          </div>
        </div>
      </nav>
    </header>
  @yield('content')
  <footer>
      <div id="footer-s1" class="footer-s1">
          <div class="footer ">
              <div class="container">
                  <div class="row  ">

                      <div class="col-md-12 col-sm-12 ">
                    <img src="{{ asset('/img/nile-angels(250x50).png')}}"width="200px;"><br><br>
                    <address class="address-details-f">
                    @lang('arabic.phone'): @lang('arabic.phone3')
                    <br> @lang('arabic.email'): <a href="mailto:info@nileangels.com" class="">info@nileangels.com</a>
                    </address>
                <div ><a href="‫‪/‬‬ ‫‪https://www.facebook.com/Nileangels‬‬" target="_empty"><i class="fa fa-facebook"></i></a></div>
                   <a href="mailto:info@nileangels.com" class="">www.nileangels.com</a>

                <div id="footer-copyrights">
                      <p>Copyrights &copy; 2018 All Rights Reserved by<a href="http://www.codexit.net/">Codexit </p>
                </div>
                      </div>

                  </div>
              </div>
              <!--/container -->
          </div>
      </div>

      <!-- <div id="footer-bottom">
          <div class="container">
              <div class="row">
                  <div class="col-md-12">

                  </div>
              </div>
          </div>
      </div> -->



  </footer>

  <!-- Global JavaScript -->
  <script src="{{ asset('/js/jquery.min.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{ asset('/js/popper.min.js')}}"></script>
<script src="{{ asset('/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('/js/jquery.easing.min.js')}}"></script>
<script src="{{ asset('/js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('/js/wow.min.js')}}"></script>
 <script src="{{ asset('/js/basic_form.js')}}"></script>
</body>

</html>
