@extends("master")
@section("content")
<div >
<div id="home-p" class="home-p pages-head3 text-center">
      <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.news')</h1>

      </div><!--/end container-->
    </div>

<!--====================================================
                        news-p1
======================================================-->
<section id="news-p1" class="news-p1" style="padding-top:10px;">
  <div class="container">
    <h1 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#3C9CF3;">@lang('arabic.news2')</h1>
        <div class="heading-border-light"></div>
    <div class="row">
        @foreach($objects as $news)
        <div class=" col-lg-4 col-md-4 col-sm-6 col-xs-12" >
          <div class="desc-comp-offer-cont" >
          <div class="thumbnail-blogs">
              <div class="caption">
                <i class="fa fa-chain"></i>
              </div>
              @if($news->mainImg)
                <img  src="{{asset('/NewsPictures')}}/{{$news->mainImg->pic}}" class="img-fluide" >
              @endif
          </div>
          <div class="card">
          <h3>{{$news->title}}</h3>
          <form class="form-horizontal" enctype="multipart/form-data" action="news_details" method="get">
            <input type="hidden" name="id" value="{{$news->id}}">
          <button  class="btn btn-general " style="width:auto;" role="button"type="submit"><i class="fa fa-arrow-circle-o-right" ></i> @lang('arabic.showmore')</button>
        </form>
        </div>
          </div>
        </div>
@endforeach
</div>
    </div>
</section>





<section id="news-p1" class="news-p1" style="padding-top:10px;">
  <div class="container">
    <h1 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#3C9CF3;">@lang('arabic.events')</h1>
     <div class="heading-border-light"></div>
    <div class="row">
        @foreach($events as $event)
        <div class=" col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="desc-comp-offer-cont">
          <div class="thumbnail-blogs">
              <div class="caption">
                <i class="fa fa-chain"></i>
              </div>
              <img src="{{asset('/events')}}/{{$event->pictures[0]->pic}}" class="img-fluide" alt="...">
          </div>

          <div class="card">
          <h3>{{$event->title}}</h3>
          <form class="form-horizontal" enctype="multipart/form-data" action="event_details" method="get">
            <input type="hidden" name="id" value="{{$event->id}}">
          <button class="btn btn-general " style="width:auto;" role="button" type="submit"><i class="fa fa-arrow-circle-o-right" ></i> @lang('arabic.showmore')</button>
        </form >
        </div>
          </div>
        </div>
@endforeach
      </div>
    </div>
</section>




<script>
function getarticle(id)
{
      $.ajax({
        url: "{{ URL::to('') }}",
        type: "post",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"id":id},
        success: function(response)
        {
           alert("hi");

        },
        error: function () {
            alert("error");
        }
      });
}
function getevent(id)
{
      $.ajax({
        url: "{{ URL::to('') }}",
        type: "post",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"id":id},
        success: function(response)
        {
           alert("hi2");

        },
        error: function () {
            alert("error");
        }
      });
}
</script>
    @stop
