@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">News</h2>
<button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#addnewsModal"><i class="fa fa-plus"></i> Add news</button>
</div>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="150">
  <col width="150">
  <col width="350">
  <col width="250">
  <col width="150">
  <col width="50">
  <col width="50">
    <thead>
      <tr >

        <th>Title</th>
        <th>Main picture</th>
        <th>Pictures</th>
        <th>Description</th>
        <th>Link</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @foreach($objects as $news)

                  <td>{{$news->title}} <br/></td>
                  <td>
                    @if($news->mainImg)
                      <img class="imge" src="{{asset('/NewsPictures')}}/{{$news->mainImg->pic}}" height="50" width = "50">
                    @endif
                  </td>
                  <td>
                  @foreach($news->pictures as $picture)
                   <a href="#" title="Delete photo" onclick="delet_photo({{$picture->id}},{{$news->id}})">X</a>
                   <img class="imge" src="{{asset('/NewsPictures')}}/{{$picture->pic}}" height="50" width = "50">
                  @endforeach
                  </td>
                  <td>{{$news->description}}</td>
                  <td>{{$news->link}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editnewsModal"  onclick = "edit_news('{{$news->id}}');" id="news_Edit" ><i class="fa fa-edit"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletenewsModal"  onclick = "delete_news('{{$news->id}}');" id="news_delete" >X</button></td>
              </tr>


        @endforeach


    </tbody>
  </table>

</div>
  <div class="modal fade" id="addnewsModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Add news</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body">

       <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('NewsController@store') }}" method = "post">

<div class="form-group row">
    <label class="control-label col-sm-4" >Title :</label>
<div class="col-sm-8">
  <input type="text" class="form-control" id="title" required name = "title" onblur="validatetitle()">
  <span id="title_invalid"></span>
</div>


</div>
<div class="form-group row">
    <label class="control-label col-sm-4"> Main picture:</label>
<div class="col-sm-8">
  <input class="center-block" type="file" id="mainimage" name="mainImg" required onchange="checkextension();"/>
  <span id="invalid_mainimage"></span>
</div>

</div>

<div class="form-group row">
    <label class="control-label col-sm-4">Pictures :</label>
<div class="col-sm-8">
  <input class="center-block" type="file" id="image" name="pictures[]" multiple="multiple" onchange="checkextension();"/>
  <span id="invalid_image"></span>
</div>

</div>

<div class="form-group row">
    <label class="control-label col-sm-4">Description :</label>
<div class="col-sm-8">
  <input type="text" class="form-control" id="description" name = "description" required onblur="validatedes()">
  <span id="invalid_des"></span>
</div>
</div>

<div class="form-group row">
  <label class="control-label col-sm-4">Link :</label>
  <div class="col-sm-8">
    <input type="url" class="form-control" id="link" name = "link" required onblur="validatelink()">
    <span id="invalid_link"></span>
  </div>

</div>

<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" col-sm-12">
  <button type="submit" class="btn btn-blue pull-right" onclick="return validatenews();"  id="add_save" >Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="editnewsModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit this news</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('NewsController@update') }}" method = "post">

 <div class="form-group row">
     <label class="control-label col-sm-4" >Title :</label>
 <div class="col-sm-8">
   <input type="text" class="form-control" id="oldtitle" required name = "title" onblur="">
   <span id="newtitle_invalid"></span>
 </div>


 </div>
 <div class="form-group row">
     <label class="control-label col-sm-4">Main picture :</label>
 <div class="col-sm-8">
   <input class="center-block" type="file" id="newmainimage" name="mainImg" onchange="checkextension();"/>
   <span id="newinvalid_mainimage"></span>
 </div>

 </div>
 <div class="form-group row">
     <label class="control-label col-sm-4">Pictures :</label>
 <div class="col-sm-8">
   <input class="center-block" type="file" id="newimage" name="pictures[]" multiple="multiple" onchange="checkextension();"/>
   <span id="newinvalid_image"></span>
 </div>

 </div>

 <div class="form-group row">
     <label class="control-label col-sm-4">Description :</label>
 <div class="col-sm-8">
   <input type="text" class="form-control" id="olddescription" name = "description" required onblur="">

 </div>
 </div>

 <div class="form-group row">
   <label class="control-label col-sm-4">Link :</label>
   <div class="col-sm-8">
     <input type="url" class="form-control" id="oldlink" name = "link" required onblur="">

   </div>

 </div>
<input type="hidden" name="id" id="editnews_id">
 <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
 <div class="form-group">
 <div class=" col-sm-12">
   <button type="submit" class="btn btn-blue pull-right"  id="add_save" >Save</button>
 </div>
 </div>
 </form>
    </div>

  </div>
</div>

</div>


<div class="modal fade" id="deletenewsModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Are you sure ?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this news
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('NewsController@destroy') }}" method = "post">


          <input type="hidden" name="id" id="news_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>

    function delete_news (id)
    {

      $("#news_id").val(id);
    }
    function delet_photo(photo_id,news_id)
    {
      $.ajax({
        url: "{{ URL::to('/news/deletePhoto') }}",
        type: "post",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"photoId":photo_id,"id":news_id},
        success: function()
        {
          window.location.href = "{{ url('admin/news') }}";
        },
        error: function () {
            alert("error");
        }
      });
    }
    function edit_news(id)
    {
          $("#editnews_id").val(id);
          $.ajax({
            url: "{{ URL::to('/news/show') }}",
            type: "post",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#oldtitle").val(response.title),
               $("#olddescription").val(response.description),
               $("#oldlink").val(response.link)

            },
            error: function () {
                alert("error");
            }
          });
    }
function checkextension() {
  var images = document.getElementById("image");
  var newimages = document.getElementById("newimage");
  var newmainimages = document.getElementById("newmainimage");
if ('files' in images) {
    if (images.files.length == 0) {
        $("#invalid_image").html("Select one or more images.");
    } else {
        for (var i = 0; i < images.files.length; i++) {
            // var file = x.files[i];
              if ( /\.(jpe?g|png)$/i.test(images.files[i].name) === false )
            {
                $("#invalid_image").html("one of files not an image !")
            }

        }
    }
}
if ('files' in newimages) {
    if (newimages.files.length == 0) {
        $("#newinvalid_image").html("Select one or more images.");
    } else {
        for (var i = 0; i < newimages.files.length; i++) {
            // var file = x.files[i];
              if ( /\.(jpe?g|png)$/i.test(newimages.files[i].name) === false )
            {
                $("#newinvalid_image").html("one of files not an image !")
            }

        }
    }
}
if ('files' in newmainimages) {
    if (newimages.files.length == 0) {
        $("#newinvalid_mainimage").html("Select an image.");
    } else {
        for (var i = 0; i < newimages.files.length; i++) {
            // var file = x.files[i];
              if ( /\.(jpe?g|png)$/i.test(newimages.files[i].name) === false )
            {
                $("#newinvalid_mainimage").html("it is not an image !")
            }

        }
    }
}

}
  </script>
@stop
