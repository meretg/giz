@extends("master")
@section("content")
<div id="home-p" class="home-p pages-head1 text-center">
      <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.details')</h1>

      </div><!--/end container-->
    </div>

<!--====================================================
                      NEWS DETAILS
======================================================-->
    <section id="single-news-p1" class="single-news-p1">
      <div class="container">
        <div class="row">

          <!-- left news details -->
          <div class="col-md-8">
            <div class="single-news-p1-cont">
              <div class="single-news-img">
                <img src="{{asset('/NewsPictures')}}/{{$news->mainImg->pic}}" alt=""  >
              </div>
              <div class="single-news-desc">
                <h3 style="color:#3C9CF3">{{$news->title}}</h3>

                <hr>
                <div class="">
                  <p style="font-weight:400%;font-size:25px;">{{$news->description}}</p>


                </div>

              </div>
            </div>

          </div>

          <!-- Right news details -->
          <div class="col-md-4">


            <div class="ad-box-sn" >

              @foreach($news->pictures as $picture)
              <div>
                <div class="desc-comp-offer-cont">
                <div class="thumbnail-blogs">
                    <div class="caption">
                      <i class="fa fa-chain"></i>
                    </div>
                    <img src="{{asset('/NewsPictures')}}/{{$picture->pic}}" class="img-fluide" alt="..." >
                </div>

                </div>
              </div>
              @endforeach

            </div>
          </div>
        </div>
      </div>
    </section>
    @stop
