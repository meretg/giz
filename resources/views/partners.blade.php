@extends("master")
@section("content")

<div id="home-p" class="home-p pages-head1 text-center">
  <div class="container">
    <h1 class="wow fadeInUp" data-wow-delay="0.1s">We offer a huge opportunity for growth</h1>
    <p>Discover more about Team</p>
  </div><!--/end container-->
</div>

<!--====================================================
                  TEAM-P1
======================================================-->
<section id="team-p1" class="team-p1">
  <div class="container">
    <div class="row">
       <div class="col-md-6">
         <div class="team-p1-cont wow fadeInUp" data-wow-delay="0.3s">
           <img src="img/team/t-2.jpg" class="img-fluid" alt="...">
           <h5>Lena boss</h5>
           <h6>CEO & Cofounder</h6>
           <ul class="list-inline social-icon-f top-data">
              <li><a href="#" target="_empty"><i class="fa top-social fa-facebook"></i></a></li>
              <li><a href="#" target="_empty"><i class="fa top-social fa-google-plus"></i></a></li>
            </ul>
          </div>
       </div>
       <div class="col-md-6">
         <div class="team-p1-cont wow fadeInUp" data-wow-delay="0.6s">
           <img src="img/team/t-5.jpg" class="img-fluid" alt="...">
           <h5>Rohn Soj</h5>
           <h6>Manager &amp; Cofounder</h6>
           <ul class="list-inline social-icon-f top-data">
              <li><a href="#" target="_empty"><i class="fa top-social fa-facebook"></i></a></li>
              <li><a href="#" target="_empty"><i class="fa top-social fa-google-plus"></i></a></li>
            </ul>
          </div>
       </div>
    </div>
  </div>
</section>

<!--====================================================
                  TEAM-P2
======================================================-->
<section id="comp-offer" class="team-p2">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6  desc-comp-offer">
        <h2>Team Member</h2>
        <div class="heading-border-light"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-6 ">
        <div class="team-p2-cont wow fadeInUp" data-wow-delay="0.2s">
           <img src="img/team/t-1.jpg" class="img-fluid" alt="...">
           <h5>Seena zell</h5>
           <h6>CEO & Cofounder</h6>
           <ul class="list-inline social-icon-f top-data">
              <li><a href="#" target="_empty"><i class="fa top-social fa-facebook"></i></a></li>
              <li><a href="#" target="_empty"><i class="fa top-social fa-google-plus"></i></a></li>
            </ul>
          </div>
      </div>
      <div class="col-md-4 col-sm-6 ">
        <div class="team-p2-cont wow fadeInUp" data-wow-delay="0.4s">
           <img src="img/team/t-3.jpg" class="img-fluid" alt="...">
           <h5>Tom Gate</h5>
           <h6>CEO & Cofounder</h6>
           <ul class="list-inline social-icon-f top-data">
              <li><a href="#" target="_empty"><i class="fa top-social fa-facebook"></i></a></li>
              <li><a href="#" target="_empty"><i class="fa top-social fa-google-plus"></i></a></li>
            </ul>
          </div>
      </div>
      <div class="col-md-4 col-sm-6 ">
        <div class="team-p2-cont wow fadeInUp" data-wow-delay="0.6s">
           <img src="img/team/t-4.jpg" class="img-fluid" alt="...">
           <h5>Jesia Ben</h5>
           <h6>CEO & Cofounder</h6>
           <ul class="list-inline social-icon-f top-data">
              <li><a href="#" target="_empty"><i class="fa top-social fa-facebook"></i></a></li>
              <li><a href="#" target="_empty"><i class="fa top-social fa-google-plus"></i></a></li>
            </ul>
          </div>
      </div>
    </div>
  </div>
</section>


<!--====================================================
                CONTACT HOME
======================================================-->
<div class="overlay-contact-h"></div>
<section id="contact-h" class="bg-parallax contact-h-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="contact-h-cont">
          <h3 class="cl-white">Continue The Conversation</h3><br>
          <form>
            <div class="form-group cl-white">
              <label for="name">Your Name</label>
              <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name">
            </div>
            <div class="form-group cl-white">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group cl-white">
              <label for="subject">Subject</label>
              <input type="text" class="form-control" id="subject" aria-describedby="subjectHelp" placeholder="Enter subject">
            </div>
            <div class="form-group cl-white">
              <label for="message">Message</label>
              <textarea class="form-control" id="message" rows="3"></textarea>
            </div>
            <button class="btn btn-general btn-white" role="button"><i fa fa-right-arrow></i>GET CONVERSATION</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!--====================================================
                   NEWS
======================================================-->
<section id="comp-offer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3 col-sm-6  desc-comp-offer wow fadeInUp" data-wow-delay="0.2s">
        <h2>Latest News</h2>
        <div class="heading-border-light"></div>
        <button class="btn btn-general btn-green" role="button">See More</button>
      </div>
      <div class="col-md-3 col-sm-6 desc-comp-offer wow fadeInUp" data-wow-delay="0.4s">
        <div class="desc-comp-offer-cont">
          <div class="thumbnail-blogs">
              <div class="caption">
                <i class="fa fa-chain"></i>
              </div>
              <img src="img/news/news-1.jpg" class="img-fluid" alt="...">
          </div>
          <h3>Pricing Strategies for Product</h3>
          <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from Business box. </p>
          <a href="#"><i class="fa fa-arrow-circle-o-right"></i> Learn More</a>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 desc-comp-offer wow fadeInUp" data-wow-delay="0.6s">
        <div class="desc-comp-offer-cont">
          <div class="thumbnail-blogs">
              <div class="caption">
                <i class="fa fa-chain"></i>
              </div>
              <img src="img/news/news-9.jpg" class="img-fluid" alt="...">
          </div>
          <h3>Design Exhibitions of 2017</h3>
          <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from Business box. </p>
          <a href="#"><i class="fa fa-arrow-circle-o-right"></i> Learn More</a>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 desc-comp-offer wow fadeInUp" data-wow-delay="0.8s">
        <div class="desc-comp-offer-cont">
          <div class="thumbnail-blogs">
              <div class="caption">
                <i class="fa fa-chain"></i>
              </div>
              <img src="img/news/news-12.jpeg" class="img-fluid" alt="...">
          </div>
          <h3>Exciting New Technologies</h3>
          <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from Business box. </p>
          <a href="#"><i class="fa fa-arrow-circle-o-right"></i> Learn More</a>
        </div>
      </div>
    </div>
  </div>
</section>



<!-- Global JavaScript -->
<script src="js/jquery/jquery.min.js"></script>
<script src="js/popper/popper.min.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/wow/wow.min.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>

<!-- Plugin JavaScript -->
<script src="js/jquery-easing/jquery.easing.min.js"></script>

<script src="js/custom.js"></script>

</body>

</html>

@stop
