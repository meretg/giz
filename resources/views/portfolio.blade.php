@extends("master")
@section("content")
<div id="home-p" class="home-p pages-head2 text-center">
    <div class="container">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Innovation Project</h1>
      <p>New Idea</p>
    </div><!--/end container-->
  </div>



<!--====================================================
                      PROJECT-P2
======================================================-->
  <section id="project-p2" class="project-p2" style="">
    <div class="container-fluid">
      <div class="row">
        @foreach($portfolios as $portfolio)
          <div class="col-md-6">
            <div class="project-p2-cont">
              <div class="col-md-5 col-sm-6">
                <div class="project-p2-desc wow fadeInUp" data-wow-delay="0.3s">
                  <h4>{{$portfolio->title}} </h4>
                </div>
              </div>
              <img src="{{asset('/PortfolioLogo')}}/{{$portfolio->logo}}" class="img-fluid" alt="...">
            </div>
          </div>
        @endforeach
        </div>
    </div>
  </section>
  @stop
