@extends("master")
@section("content")


<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img src="img/home-banner-bg.jpg" alt="Los Angeles" width="100%" height="700">
      <div class="hero ">
                    <hgroup class="wow fadeInUp">
                      <h1>Nile Angels
                        <span><h2 style="color:#007BFF;">نايل انجلز</h2></span>
                      </h1>
                      <h2>@lang('arabic.investors_cover')<h2>
                      <h2>@lang('arabic.slogan')</h2>
                    </hgroup>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/news-box.jpg" alt="Chicago" width="100%" height="700">
      <div class="hero ">
                    <hgroup class="wow fadeInUp">
                      <h1>Nile Angels
                          <span><h2 style="color:#007BFF;">نايل انجلز</h2></span>
                      </h1>
                      <h2>@lang('arabic.investors_cover')<h2>
                      <h2>@lang('arabic.slogan')</h2>
                    </hgroup>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/contact-bg.jpg" alt="New York" width="100%" height="700">
      <div class="hero ">
                    <hgroup class="wow fadeInUp">
                        <h1>Nile Angels
                            <span><h2 style="color:#007BFF;">نايل انجلز</h2></span>
                        </h1>
                        <h2>@lang('arabic.investors_cover')<h2>
                        <h2>@lang('arabic.slogan')</h2>
                    </hgroup>
      </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<!--====================================================
                        ABOUT
======================================================-->
    <section id="about" class="about">
      <div class="container">
        <div class="row title-bar">
          <div class="col-md-12">
            <h1 class="wow fadeInUp">@lang('arabic.nile')  </h1>
            <div class="heading-border"></div>

            <p class="wow fadeInUp" data-wow-delay="0.4s">@lang('arabic.content1') </p>
          </div>
        </div>
      </div>
      <!-- About right side withBG parallax -->
      <div class="container">
        <div class="row title-bar">
          <div class="col-md-12">
        <h1 class="wow fadeInUp">@lang('arabic.startups')  </h1>
            <div class="heading-border"></div>
            <!-- <h3 class="row" style="margin-right:20%;"> @lang('arabic.invest') </h3> -->
              <p class="wow fadeInUp" data-wow-delay="0.4s">@lang('arabic.stratuphome') </p>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <center style="font-size:55px ;color:#41464b; font-weight:700px;" >@lang('arabic.services')</center>
          <div class="heading-border"> <br> <br></div>
        <div class="row">

          <div class="col-md-4 bg-starship">

            <div class="about-content-box wow fadeInUp" data-wow-delay="0.3s">
              <i class="fa fa-comments"></i>
              <h5>@lang('arabic.consalt')</h5>
              <p class="desc">@lang('arabic.consaltcontent')</p>

</p>
            </div>
          </div>
          <div class="col-md-4 bg-chathams">
            <div class="about-content-box wow fadeInUp" data-wow-delay="0.5s">
              <i class="fa fa-sign-out"></i>
              <h5>@lang('arabic.directing') </h5>
            <p class="desc">@lang('arabic.directingcontent')</p>
            </div>
          </div>
          <div class="col-md-4 bg-matisse">
            <div class="about-content-box wow fadeInUp" data-wow-delay="0.7s">
              <i style="color:white;" class="fa fa-users"></i>
              <h5>@lang('arabic.socity') </h5>
              <p class="desc">@lang('arabic.socitycontent')</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center align-items-center shadow">
      <a  href="{{ url('en/investor') }}" class="btn btn-general btn-green shadow" style="width:auto;" role="button">@lang('arabic.joininvestor')</a>
      <a  href="{{ url('en/investor') }}" class="btn btn-general btn-green shadow" style="width:auto;" role="button">@lang('arabic.joinenter')</a>
      </div>
    </section>



<!--====================================================
                       NEWS
======================================================-->
    <section id="comp-offer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3 col-sm-6  desc-comp-offer wow fadeInUp" data-wow-delay="0.2s">
            <h2>@lang('arabic.client')</h2>
            <div class="heading-border-light"></div>

          </div>
          <div class="col-md-3 col-sm-6 desc-comp-offer wow fadeInUp" data-wow-delay="0.4s">
            <div class="desc-comp-offer-cont">
              <div class="thumbnail-blogs">
                  <div class="caption">
                    <i class="fa fa-chain"></i>
                  </div>
                  <img src="img/partner1.png"  class="img-fluid" alt="...">
              </div><hr>
              <center>‫‪GIZ‬‬</center>
              <p class="desc">‬‬</p>

            </div>
          </div>


        </div>
      </div>
    </section>

<!--====================================================
                      FOOTER
======================================================-->
@stop
