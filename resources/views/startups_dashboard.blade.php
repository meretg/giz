@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Start up</h2>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="200">
  <col width="250">
  <col width="150">
  <col width="300">
  <col width="200">
  <col width="200">
    <thead>
      <tr >

        <th>Fullname</th>
        <th>Email</th>
        <th>City</th>
        <th>Address of start up</th>
        <th>View</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr >
        @foreach($Startups as $startup)
                  <td>{{$startup["applicant"]->fullName}} <br/></td>
                  <td>{{$startup["applicant"]->email}}</td>
                  <td>{{$startup["applicant"]->city}}</td>
                  <td>{{$startup->address}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#viewstartupModal"  onclick = "startup_view('{{$startup->id}}');" id="startup_Edit" ><i class="fa fa-eye"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletestartupModal"  onclick = "delete_startup('{{$startup->id}}');" id="startup_delete" >X</button></td>
              </tr>
        @endforeach

    </tbody>
  </table>
  <div class="modal fade" id="viewstartupModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">View this startup</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body">
            <form class="form-horizontal">

  <div class="form-group row">
    <label class="control-label col-sm-4" >fullname :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="fullname" name="fullName"></label>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Email :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="email" name="email"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Phone number :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="phone" name="phoneNo"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-8" >Another phone no. or whatsapp no. :</label>
    <div class="col-sm-4">
      <label class="text-primary" id="whats_phone" name="whatsUpNo"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Age :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="age" name="age"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >City :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="city" name="city"></label>

    </div>
  </div>


  <div class="form-group row">
    <label class="control-label col-sm-4" >Background :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="background" name="background"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Number of employee :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="experience" name="employeeNo"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-5" >Team of start up :</label>
    <div class="col-sm-7">
      <label class="text-primary" id="team" name="team"></label>

    </div>
  </div>

  <div class="form-group row">
    <label class="control-label col-sm-6" >Description of start up :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="description" name="ideaDescription"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Customers of start up :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="customers" name="customers"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Launch date of start up :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="launch_date" name="launchDate"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Registered :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="registered" name="registered"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-6" >Provide to customers :</label>
    <div class="col-sm-6">
      <label class="text-primary" id="Provide_customers" name="customers"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-5" >Facebook page :</label>
    <div class="col-sm-7">
      <label class="text-primary" id="facebook" name="facebookPage"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-3" >Website :</label>
    <div class="col-sm-9">
      <label class="text-primary" id="website" name="website"></label>

    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-4" >Business Plan :</label>
    <div class="col-sm-8">
      <label class="text-primary" id="business_plan" name="bussinessPlan"></label>

    </div>
  </div>
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="viewinvestor_id">
  <div class="form-group">
    <div class=" col-sm-12">
      <button  id="save_update" class="btn btn-blue pull-right"data-dismiss="modal" >Ok</button>
    </div>
  </div>
        </form>
      </div>

    </div>
  </div>

  </div>

  <div class="modal fade" id="deletestartupModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you sure ?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>


        </div>
        <div class="modal-body">
         <strong style="color:red;">
             You will delete all information about this start up
         </strong>
        </div>
        <div class="modal-footer">

          <form class="form-horizontal" action = "{{ route('delete_startup') }}" method = "post">
            <input type="hidden" name="id" id="startup_id">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
            <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
          </form>


        </div>
      </div>

    </div>
  </div>
  </section>
  <script>

      function delete_startup (id)
      {
        $("#startup_id").val(id);
      }
      function startup_view(id)
  {
          $("#viewinvestor_id").val(id);
            $.ajax({
              url: "{{ URL::to('startup.view') }}",
              type: "get",
              dataType: 'json',
              data: {"_token":$('#_token').val(),"id":id},
              success: function(response)
              {
                 $("#fullname").html(response["applicant"].fullName),
                 $("#email").html(response["applicant"].email),
                 $("#phone").html(response["applicant"].phoneNo),
                 $("#whats_phone").html(response["applicant"].whatsUpNo),
                 $("#age").html(response["applicant"].age),
                 $("#city").html(response["applicant"].city),
                 $("#background").html(response["applicant"].background),
                 $("#team").html(response.teamDetails),
                 $("#employee_No").html(response.employeeNo),
                 $("#description").html(response["applicant"].ideaDescription),
                 $("#customers").html(response.customers),
                 $("#launch_date").html(response.launchDate),
                 $("#registered").html(response.registered),
                 $("#provide_customers").html(response.services),
                 $("#facebook").html(response["applicant"].facebookPage),
                 $("#website").html(response["applicant"].website),
                 $("#business_plan").html(response["applicant"].bussinessPlan)
                 },


              error: function () {

                  alert("error");

              }
              });
  }
    </script>
</div>
</section>
@stop
