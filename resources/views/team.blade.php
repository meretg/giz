@extends("master")
@section("content")
<div style="background-color:white;">
<div id="home-p" class="home-p pages-head1 text-center">
      <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">@lang('arabic.team')</h1>

      </div><!--/end container-->
    </div>

<!--====================================================
                      TEAM-P1
======================================================-->

<section id="comp-offer" class="team-p2">
     <div class="container">
       <div class="row">
         <div class="col-md-4 col-sm-6  desc-comp-offer">
         <h2>@lang('arabic.investor_team')</h2>
           <div class="heading-border-light"></div>
         </div>
       </div>
       <div class="row">
         @foreach($team as $member)
         @if($member->type=="investor")
         <div class="col-md-4 col-sm-6 ">
           <div class="team-p2-cont wow fadeInUp" data-wow-delay="0.2s">
             <div class="row justify-content-center align-items-center">
            <img src="{{ asset('../storage/app')}}/{{$member->pic}}"   class=" team_image"   alt="...">
          </div>
            <h5 class="text-center">{{ $member->name }}</h5>
            <h6 class="text-center">{{ $member->bio }}</h6>
               <div class="row justify-content-center align-items-center">
              <a href="#" target="_empty"><i class="fa top-social fa-facebook-square"></i></a>
              <a href="#" target="_empty"><i class="fa top-social fa-linkedin-square"></i></a>
            </div>
            </div>
           <div>
             <br>
           </div>
           </div>
       @endif
         @endforeach
           </div>
           </div>
   </section>








   <section id="comp-offer" class="team-p2">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-6  desc-comp-offer">
            <h2>@lang('arabic.executive_team')</h2>
              <div class="heading-border-light"></div>
            </div>
          </div>
          <div class="row">
            @foreach($team as $member)
            @if($member->type=="executive")
            <div class="col-md-4 col-sm-6 ">
              <div class="team-p2-cont wow fadeInUp" data-wow-delay="0.2s">
                <div class="row justify-content-center align-items-center">
               <img src="{{ asset('../storage/app')}}/{{$member->pic}}"   class=" team_image"   alt="...">
             </div>
               <h5 class="text-center">{{ $member->name }}</h5>
               <h6 class="text-center">{{ $member->bio }}</h6>
                  <div class="row justify-content-center align-items-center">
                 <a href="#" target="_empty"><i class="fa top-social fa-facebook-square"></i></a>
                 <a href="#" target="_empty"><i class="fa top-social fa-linkedin-square"></i></a>
               </div>
               </div>
              <div>
                <br>
              </div>
              </div>
          @endif
            @endforeach
              </div>
              </div>
      </section>





@stop
