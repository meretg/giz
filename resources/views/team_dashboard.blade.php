@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">Team</h2>
<button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#addmemberModal"><i class="fa fa-plus"></i> Add member</button>
</div>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="100">
  <col width="100">
  <col width="200">
  <col width="100">
  <col width="150">
  <col width="100">
  <col width="150">
  <col width="150">
  <col width="50">
  <col width="50">
    <thead>
      <tr >

        <th>Name</th>
        <th>Photo</th>
        <th>Bio</th>
        <th>Type</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Facebook</th>
        <th>Linkedin</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr >
        @foreach($teams as $member)

                  <td>{{$member->name}} <br/></td>
                  <td><img class="imge" src="{{asset('../storage/app')}}/{{$member->pic}}" style="width:40px; height:40px;"></td>
                  <td>{{$member->bio}}</td>
                  <td>{{$member->type}}</td>
                  <td>{{$member->email}}</td>
                  <td>{{$member->phone}}</td>
                  <td>{{$member->facebook}}</td>
                  <td>{{$member->linkedin}}</td>
                  <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editmemberModal"  onclick = "edit_member('{{$member->id}}');" id="member_Edit" ><i class="fa fa-edit"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletememberModal"  onclick = "delete_member('{{$member->id}}');" id="member_delete" >X</button></td>
              </tr>


        @endforeach

    </tbody>
  </table>

</div>
  <div class="modal fade" id="addmemberModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Add member</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body">

       <form class="form-horizontal" enctype="multipart/form-data" action = "{{ URL('add_member') }}" method = "post">

<div class="form-group row">
    <label class="control-label col-sm-3" >Name :</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="member_name" required name = "name" onblur="validatename_member()">
  <span id="member_nameinvalid"></span>
</div>


</div>

<div class="form-group row">
    <label class="control-label col-sm-3">Photo :</label>
<div class="col-sm-9">
  <input class="center-block" type="file" id="image" name="photo" onchange="checkextension();"/>
  <span id="invalid_image"></span>
</div>

</div>

<div class="form-group row">
    <label class="control-label col-sm-3">Bio :</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="member_bio" name = "bio" required onblur="">

</div>
</div>

<div class="form-group row" >
  <label class="control-label col-sm-3" >Type :</label>
  <div class="col-sm-9">
  <div class="btn-group">
  <select id="member_type" class=" custom-select select_type" name="type">
    <option>Investor</option>
    <option>Executive</option>
  </select>
  </div>
</div>
</div>


  <div class="form-group row">
    <label class="control-label col-sm-3">Email :</label>
    <div class="col-sm-9">
      <input type="emial" class="form-control" id="member_email" name = "email" required onblur="validateEmail_member()">
      <span id="member_emailinvalid"></span>
    </div>

</div>
<div class="form-group row">
  <label class="control-label col-sm-3">Phone :</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="member_phone" name = "phone" required onblur="validatephone_member()">
    <span id="member_phoneinvalid"></span>
  </div>

</div>

<div class="form-group row">
  <label class="control-label col-sm-3">Facebook:</label>
  <div class="col-sm-9">
    <input type="url" class="form-control" id="member_facebook" name = "facebook">

  </div>

</div>

<div class="form-group row">
  <label class="control-label col-sm-3">Linkedin :</label>
  <div class="col-sm-9">
    <input type="url" class="form-control" id="member_linkedin" name = "linkedin">

  </div>

</div>

<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" col-sm-12">
  <button type="submit" class="btn btn-blue pull-right" onclick="return validate_team()"  id="add_save" >Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<div class="modal fade" id="editmemberModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit this member</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
          <form class="form-horizontal" enctype="multipart/form-data" action = "{{ URL('edit_member') }}" method = "post">


            <div class="form-group row">
                <label class="control-label col-sm-3" >Name :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="oldmember_name" required name = "name" onblur="">

            </div>


            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">Photo :</label>
            <div class="col-sm-9">
              <input class="center-block" type="file" id="image" name="photo" onchange="checkextension();"/>
              <span id="invalid_image"></span>
            </div>

            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">Bio :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="oldmember_bio" name = "bio" required onblur="">

            </div>
          </div>

            <div class="form-group row" >
              <label class="control-label col-sm-3" >Type :</label>
              <div class="col-sm-9">
              <div class="btn-group">
              <select class="custom-select select_type" id="oldmember_type" name="type">
                <option>Investor</option>
                <option>Executive</option>
              </select>
              </div>
            </div>
            </div>


              <div class="form-group row">
                <label class="control-label col-sm-3">Email :</label>
                <div class="col-sm-9">
                  <input type="emial" class="form-control" id="oldmember_email" name = "email" required onblur="">

                </div>

            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Phone :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="oldmember_phone" name = "phone" required onblur="">

              </div>

            </div>

            <div class="form-group row">
              <label class="control-label col-sm-3">Facebook:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="oldmember_facebook" name = "facebook" required onblur="">

              </div>

            </div>

            <div class="form-group row">
              <label class="control-label col-sm-3">Linkedin :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="oldmember_linkedin" name = "linkedin" required onblur="">
              </div>

            </div>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editmember_id">
<div class="form-group">
  <div class=" col-sm-12">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-blue pull-right"data-dismiss="modal" >Save</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deletememberModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Are you sure ?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this member
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ URL('delete_member') }}" method = "get">


          <input type="hidden" name="id" id="member_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>
$('.select_type').multiselect({
    enableHTML: true,
    templates: {
        li: '<li><a href="javascript:void(0);"><label class="pl-2 w-100"></label></a></li>',
        button: '<button type="button" class="multiselect w-100 text-left" data-toggle="dropdown">Select..</button>',
    },
    buttonContainer: '<div class="w-100" />',
    buttonClass: 'btn btn-outline-primary',
    selectedClass: 'bg-light',
    onInitialized: function(select, container) {
        // hide radio
        container.find('input[type=radio]').addClass('d-none');
    }
});
    function delete_member (id)
    {

      $("#member_id").val(id);
    }
    function edit_member(id)
{
          $("#editmember_id").val(id);
          $.ajax({
            url: "{{ URL::to('view.member') }}",
            type: "put",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#oldmember_name").val(response.name),
               $("#oldmember_email").val(response.email),
               $("#oldmember_phone").val(response.phone),
               $("#oldmember_facebook").val(response.facebook),
               $("#oldmember_linkedin").val(response.linkedin),
               $("#oldmember_bio").val(response.bio),
               $("#oldmember_photo").val(response.photo),
               $("#oldmember_type").select(response.type)
               },


            error: function () {

                alert("error");

            }
            });
}
function checkextension() {
  var image = document.querySelector("#image");
  var oldimage=document.querySelector("#oldmember_image");
  // document.querySelector("#image").height="260";
  // document.querySelector("#image").width="280";
  if ( /\.(jpe?g|png)$/i.test(image.files[0].name) === false )
   {
   $("#invalid_image").html("Not an image !");
  }
  else {
    $("#invalid_image").html("");
  }
  if ( /\.(jpe?g|png)$/i.test(oldimage.files[0].name) === false )
   {
   $("#invalid_oldimage").html("Not an image !");
  }
  else {
    $("#invalid_oldimage").html("");
  }
}
  </script>
@stop
